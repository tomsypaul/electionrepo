<?php
include '../connection.php';
require_once('../FPDF/fpdf.php');
require_once('../FPDI/src/autoload.php');
use \setasign\Fpdi\Fpdi;


$office=mysqli_query($conn,"select DISTINCT office_name from ((select  * from election_official1 where status='Allotted') UNION(select  * from election_official2 where status='Allotted') UNION (select  * from election_observer where status='Allotted'))as p");
date_default_timezone_set("Asia/Kolkata");
$date=date("d/m/Y");

generatePDF("institution.pdf","CoveringLetter.pdf","blank.pdf",$office,$conn, $date);

function generatePDF($source, $output,$blank,$office,$conn, $date)
{
      $pdf = new FPDI();


        while($data = mysqli_fetch_assoc($office))
        {

          $count=1;
          $pdf->AddPage();
          $pdf->SetFont('Arial','B',11);
           $pdf->SetXY(22.0,220);
           $pdf->Cell(12,9,'Sl. No',1,0,'C');
           $pdf->Cell(49,9,'Name',1,0,'C');
           $pdf->Cell(54,9,'Designation',1,0,'C');
           $pdf->Cell(30,9,'Category',1,0,'C');
           $pdf->Cell(25,9,'Signature',1,0,'C');
                   $sql3=mysqli_query($conn,"Drop view if exists view_data");
         					$sql5=mysqli_query($conn,"CREATE VIEW view_insti AS (SELECT CONCAT('off1',official1_id) as ID,p1.name,p1.designation,p1.office_name from election_official1 as p1 WHERE p1.status='Allotted')
         					 UNION
         					  (SELECT CONCAT( 'off2',official2_id) as ID ,p2.name,p2.designation,p2.office_name from election_official2 as p2 WHERE p2.status='Allotted')
         						UNION
				                (SELECT CONCAT('obse',observer_id) as ID ,p3.name,p3.designation,p3.office_name from election_observer as p3 WHERE p3.status='Allotted')");

                                $details=mysqli_query($conn,"select * from view_insti");


                              $x=228; #y-cord
        while ($row = mysqli_fetch_assoc($details))
        {

          if($row['office_name']==$data['office_name'])
          {

            $office_name =$row['office_name'];
            $designation=$row['designation'];
            $name=$row['name'];
            $id=$row['ID'];

            $sub=substr($id,0,4);

            if($sub=="off1")
              $category='Official 1';
            if($sub=="off2")
              $category='Official 2';
            if($sub=="obse")
              $category='Micro Observer';

                if($count<=5)
                {


                           //Add A4 by Default
                          $pagecount = $pdf->setSourceFile($source);
                          $tppl = $pdf->importPage(1);
                          $pdf->useTemplate($tppl, 0, 0); //need to be set , 210, 148


                          $pdf->SetFont('Arial','',11); // Font Name, Font Style (eg. 'B' for Bold), Font Size
                          $pdf->SetTextColor(0,0,0); // RGB
                          $pdf->SetXY(37,81); // X start, Y start in mm
                          $pdf->Write(0, $office_name);

                          $pdf->SetFont('Arial','',11); // Font Name, Font Style (eg. 'B' for Bold), Font Size
                          $pdf->SetTextColor(0,0,0); // RGB
                          $pdf->SetXY(154.4,43.9); // X start, Y start in mm
                          $pdf->Write(0, $date);

                          $pdf->SetFont('Arial','',10);
                          $pdf->SetXY(22.0,$x);
                          $pdf->Cell(12,9,$count,1,0,'C');
                          $pdf->Cell(49,9,$name,1,0,'L');
                          $pdf->Cell(54,9,$designation,1,0,'L');
                          $pdf->Cell(30,9,$category,1,0,'L');
                          $pdf->Cell(25,9,'',1,0,'L');


                          $count++;
                          $x=$x+8;
                      }
                      else if ($count==6)
                      {
                            $pdf->AddPage();

                            $pagecount = $pdf->setSourceFile($blank);
                            $tppl = $pdf->importPage(1);
                            $pdf->useTemplate($tppl, 0, 0); //need to be set , 210, 148

                            $y=19;
                            $pdf->SetFont('Arial','B',11);
                             $pdf->SetXY(21,$y);
                             $pdf->Cell(12,9,'Sl. No',1,0,'C');
                             $pdf->Cell(49,9,'Name',1,0,'C');
                             $pdf->Cell(54,9,'Designation',1,0,'C');
                             $pdf->Cell(32,9,'Category',1,0,'C');
                             $pdf->Cell(25,9,'Signature',1,0,'C');
                             $y=$y+8;
                             $pdf->SetFont('Arial','',10);
                            $pdf->SetXY(21,$y);
                            $pdf->Cell(12,9,$count,1,0,'C');
                            $pdf->Cell(49,9,$name,1,0,'L');
                            $pdf->Cell(54,9,$designation,1,0,'L');
                            $pdf->Cell(32,9,$category,1,0,'L');
                            $pdf->Cell(25,9,'',1,0,'L');
                            $count++;

                        }
                        else
                        {
                          $y=$y+8;
                          $pdf->SetXY(21,$y);
                          $pdf->Cell(12,9,$count,1,0,'C');
                          $pdf->Cell(49,9,$name,1,0,'L');
                          $pdf->Cell(54,9,$designation,1,0,'L');
                          $pdf->Cell(32,9,$category,1,0,'L');
                          $pdf->Cell(25,9,'',1,0,'L');
                          $count++;

                        }

            }
        }
        }
  $pdf->Output($output, "I");
}



?>
