<?php
include '../connection.php';
require_once('../FPDF/fpdf.php');
require_once('../FPDI/src/autoload.php');
use \setasign\Fpdi\Fpdi;

$lac=$_POST['lac'];
$office=mysqli_query($conn,"select DISTINCT office_name from ((select * from election_official1 where status='Allotted' and lac_no='$lac')
 UNION
 (select  * from election_official2 where status='Allotted' and lac_no='$lac')
  UNION
  (select  * from election_observer where status='Allotted' and lac_no='$lac'))as p");

$output="LAC_".$lac.".pdf";


date_default_timezone_set("Asia/Kolkata");
$date=date('d/m/Y');


generatePDF("institution.pdf", "appointment.pdf","blank.pdf",$output,$office,$conn,$lac,$date);

function generatePDF($source,$source1,$blank, $output,$office,$conn,$lac,$date )
{
      $pdf = new FPDI();

  #fetching each office name
	while($data = mysqli_fetch_assoc($office))
	{

			$count=1;

			$pdf->AddPage();


                  $pdf->SetFont('Arial','B',11);
                   $pdf->SetXY(22.0,220);
                   $pdf->Cell(12,9,'Sl. No',1,0,'C');
                   $pdf->Cell(49,9,'Name',1,0,'C');
                   $pdf->Cell(54,9,'Designation',1,0,'C');
                   $pdf->Cell(30,9,'Category',1,0,'C');
                   $pdf->Cell(25,9,'Signature',1,0,'C');

                   $sql3=mysqli_query($conn,"Drop view if exists view_data");
         					$sql5=mysqli_query($conn,"CREATE VIEW view_data AS (SELECT CONCAT('off1',official1_id) as ID,p1.name,p1.designation,p1.office_name,p1.lac_no,p1.status,p1.address1 from election_official1 as p1 WHERE p1.status='Allotted')
         					 UNION
         					  (SELECT CONCAT( 'off2',official2_id) as ID ,p2.name,p2.designation,p2.office_name,p2.lac_no,p2.status,p2.address1 from election_official2 as p2 WHERE p2.status='Allotted')
         						UNION
				                (SELECT CONCAT('obse',observer_id) as ID ,p3.name,p3.designation,p3.office_name,p3.lac_no,p3.status,p3.address1 from election_observer as p3 WHERE p3.status='Allotted')");

                                $details=mysqli_query($conn,"select * from view_data where lac_no='$lac' and status='Allotted'");


                                $x=228; #y-cordinate
			while ($row = mysqli_fetch_assoc($details))
			{
            				if($row['office_name']==$data['office_name'])
            				{


                      $office_name =$row['office_name'];

                      $designation=$row['designation'];
                      $name=$row['name'];
                      $id=$row['ID'];
                      $sub=substr($id,0,4);


                      $name = (strlen($name) > 23) ? substr($name,0,23).'-' : $name;
                      $designation = (strlen($designation) > 23) ? substr($designation,0,23).'-' : $designation;

                      if($sub=="off1")
                        $category='Official 1';
                      if($sub=="off2")
                        $category='Official 2';
                      if($sub=="obse")
                        $category='Micro Observer';

                          if($count<=5)
                          {


                          					 //Add A4 by Default
                          					$pagecount = $pdf->setSourceFile($source);
                          					$tppl = $pdf->importPage(1);
                                    $pdf->useTemplate($tppl, 0, 0); //need to be set , 210, 148


                          					$pdf->SetFont('Arial','',11); // Font Name, Font Style (eg. 'B' for Bold), Font Size
                          					$pdf->SetTextColor(0,0,0); // RGB
                          					$pdf->SetXY(37,81); // X start, Y start in mm
                          					$pdf->Write(0, $office_name);
											
											$pdf->SetFont('Arial','',11); // Font Name, Font Style (eg. 'B' for Bold), Font Size
                          					$pdf->SetTextColor(0,0,0); // RGB
                          					$pdf->SetXY(37,86); // X start, Y start in mm
                          					$pdf->Write(0, $row['address1']);

                                    $pdf->SetFont('Arial','',11); // Font Name, Font Style (eg. 'B' for Bold), Font Size
                                    $pdf->SetTextColor(0,0,0); // RGB
                                    $pdf->SetXY(154.4,43.9); // X start, Y start in mm
                                    $pdf->Write(0, $date);

                                    $pdf->SetFont('Arial','',10);
                          					$pdf->SetXY(22.0,$x);
                          					$pdf->Cell(12,9,$count,1,0,'C');
                          					$pdf->Cell(49,9,$name,1,0,'L');
                          					$pdf->Cell(54,9,$designation,1,0,'L');
                          					$pdf->Cell(30,9,$category,1,0,'L');
                          					$pdf->Cell(25,9,'',1,0,'L');


                          					$count++;
                          					$x=$x+8;
                          			}
                                else if ($count==6)
                                {
                                      $pdf->AddPage();

                                      $pagecount = $pdf->setSourceFile($blank);
                                      $tppl = $pdf->importPage(1);
                                      $pdf->useTemplate($tppl, 0, 0); //need to be set , 210, 148

                                      $y=19;
                                      $pdf->SetFont('Arial','B',11);
                                       $pdf->SetXY(21,$y);
                                       $pdf->Cell(12,9,'Sl. No',1,0,'C');
                                       $pdf->Cell(49,9,'Name',1,0,'C');
                                       $pdf->Cell(54,9,'Designation',1,0,'C');
                                       $pdf->Cell(32,9,'Category',1,0,'C');
                                       $pdf->Cell(25,9,'Signature',1,0,'C');
                                       $y=$y+8;
                                       $pdf->SetFont('Arial','',10);
                            					$pdf->SetXY(21,$y);
                            					$pdf->Cell(12,9,$count,1,0,'C');
                            					$pdf->Cell(49,9,$name,1,0,'L');
                            					$pdf->Cell(54,9,$designation,1,0,'L');
                            					$pdf->Cell(32,9,$category,1,0,'L');
                            					$pdf->Cell(25,9,'',1,0,'L');
                                      $count++;

                                  }
                                  else
                                  {
                                    $y=$y+8;
                                    $pdf->SetXY(21,$y);
                                    $pdf->Cell(12,9,$count,1,0,'C');
                                    $pdf->Cell(49,9,$name,1,0,'L');
                                    $pdf->Cell(54,9,$designation,1,0,'L');
                                    $pdf->Cell(32,9,$category,1,0,'L');
                                    $pdf->Cell(25,9,'',1,0,'L');
                                    $count++;

                                  }

                      }

	}



######################################## ALlotment Oder of each in team in particular office #######################################



		$officers=$data['office_name'];

		  $records = mysqli_query($conn,"SELECT team_id,e1.name as name1,e1.designation as designation1,e1.office_code_no as office_code_no_1 ,e1.office_name as office_name_1,e1.mobile_phone_no as mob1,
		  e2.name as name2,e2.designation as designation2,e2.office_code_no as office_code_no_2,e2.office_name as office_name_2,e2.mobile_phone_no as mob2 ,
		  e3.name as name3,e3.designation as designation3,e3.office_code_no as office_code_no_3 ,e3.office_name as office_name_3,e3.mobile_phone_no as mob3 FROM election_allotment_details
		   INNER JOIN election_official1 e1 ON e1.official1_id = election_allotment_details.official1_id
		   INNER JOIN election_official2 e2 ON e2.official2_id = election_allotment_details.official2_id
		   INNER JOIN election_observer e3 ON e3.observer_id = election_allotment_details.observer_id
		   where (e1.lac_no='$lac' or e2.lac_no='$lac' or e3.lac_no='$lac') and (e1.office_name='$officers' or e2.office_name='$officers' or e3.office_name='$officers')");

		while($ind = mysqli_fetch_assoc($records))
		{

			$team_id=$ind['team_id'];

			$team_fetch=mysqli_query($conn,"select * from election_village where village_code=
			(select village_code from election_allotment_details where team_id=$team_id)");
			$lac_fetch=mysqli_fetch_assoc($team_fetch);
			$allotted_lac=$lac_fetch['lac_number'];

			$Lac_name_fetch=mysqli_query($conn,"select * from election_nearbylac where lac_number='$allotted_lac'");
			$lac_names=mysqli_fetch_assoc($Lac_name_fetch);
			$allotted_lac_name=$lac_names['lac_name'];



		   $official1_name=$ind['name1'];
		   $official1_designation=$ind['designation1'];
		   $official1_phone_number=$ind['mob1'];
		   $official1_office_id=$ind['office_code_no_1'];
		   $official1_office_name=strtoupper($ind['office_name_1']);
					$official2_name=$ind['name2'];
		   $official2_designation=$ind['designation2'];
		   $official2_phone_number=$ind['mob2'];
		   $official2_office_id=$ind['office_code_no_2'];
		   $official2_office_name=strtoupper($ind['office_name_2']);
		   $official3_name=$ind['name3'];
		   $official3_designation=$ind['designation3'];
		   $official3_phone_number=$ind['mob3'];
		   $official3_office_id=$ind['office_code_no_3'];
		   $official3_office_name=strtoupper($ind['office_name_3']);

		   $pdf->AddPage(); //Add A4 by Default
		   $pagecount = $pdf->setSourceFile($source1);
		   $tppl = $pdf->importPage(1);

		   $pdf->useTemplate($tppl, 0, 0); //need to be set , 210, 148


		   $pdf->SetFont('Arial','I',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(181.1,174.5); // X start, Y start in mm
		   $x_teamid=$team_id;
		   if($team_id>=25 && $team_id <= 48) $x_teamid-=24;
		   else if($team_id>=49 && $team_id <= 61) $x_teamid-=48;
		   else if($team_id>=62 && $team_id <= 81) $x_teamid-=61;
		   else if($team_id>=82 && $team_id <= 96) $x_teamid-=81;
		   else if($team_id>=97 && $team_id <= 120) $x_teamid-=96;
		   else if($team_id>=121 && $team_id <= 136) $x_teamid-=120;
		   else if($team_id>=137 && $team_id <= 161) $x_teamid-=136;
		   else if($team_id>=162 && $team_id <= 192) $x_teamid-=161;
		   $pdf->Write(0, $allotted_lac."-".$x_teamid);

		   $allotted_lac=$allotted_lac." - ".$allotted_lac_name;
		   $pdf->SetFont('Arial','I',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(94,174.5); // X start, Y start in mm
		   $pdf->Write(0, $allotted_lac);


       /*
		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(65.6,150.5); // X start, Y start in mm
		   $pdf->Write(0, $place);

		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(65.6,145.5); // X start, Y start in mm
		   $pdf->Write(0, $time);

		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(65.6,140.5); // X start, Y start in mm
		   $pdf->Write(0, $date); */
       if(strlen($official1_name) > 25)
       {
         $string1 = substr($official1_name,0,25)."-";
         $pdf->SetFont('Arial','I',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
         $pdf->SetTextColor(0,0,0); // RGB
         $pdf->SetXY(80,102.3); // X start, Y start in mm
         $pdf->Write(0, strtoupper($string1));
         $official1_name_second_part = substr($official1_name,25);
         $pdf->SetXY(80,107.3); // X start, Y start in mm
         $pdf->Write(0, strtoupper($official1_name_second_part));
       }
       else {
         $pdf->SetFont('Arial','I',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
         $pdf->SetTextColor(0,0,0); // RGB
         $pdf->SetXY(80,102.3); // X start, Y start in mm
         $pdf->Write(0, strtoupper($official1_name));
       }



       if(strlen($official1_designation) > 25)
       {
         $desig = substr($official1_designation,0,25)."-";
         $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
         $pdf->SetTextColor(0,0,0); // RGB
         $pdf->SetXY(80,116.3); // X start, Y start in mm
         $pdf->Write(0, strtoupper($desig));
         $desig_name_second_part = substr($official1_designation,25);
         $pdf->SetXY(80,121.3); // X start, Y start in mm
         $pdf->Write(0, strtoupper($desig_name_second_part));
       }
       else {
         $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
         $pdf->SetTextColor(0,0,0); // RGB
         $pdf->SetXY(80,116.3); // X start, Y start in mm
         $pdf->Write(0, strtoupper($official1_designation));
       }

		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $y=0;
		   $official1_office_name_and_code = $official1_office_name."(".$official1_office_id.")";
		   while(strlen($official1_office_name_and_code) > 25)
			 {
			   $string1 = substr($official1_office_name_and_code,0,25)."-";
			   $official1_office_name_and_code = substr($official1_office_name_and_code,25);
			   $pdf->SetXY(80,130.3+$y); // X start, Y start in mm
			   $pdf->Write(0,$string1);
			   $y += 7;
			 }
		   $pdf->SetXY(80,130.3+$y); // X start, Y start in mm
		   $pdf->Write(0,$official1_office_name_and_code);


       $official1_phone_number="Ph: ".$official1_phone_number;
		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(80,158.3); // X start, Y start in mm
		   $pdf->Write(0, $official1_phone_number);



       if(strlen($official2_name) > 25)
       {
         $string2 = substr($official2_name,0,25)."-";
         $pdf->SetFont('Arial','I',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
         $pdf->SetTextColor(0,0,0); // RGB
         $pdf->SetXY(139,102.3); // X start, Y start in mm
         $pdf->Write(0, strtoupper($string2));
         $official2_name_second_part = substr($official2_name,25);
         $pdf->SetXY(139,107.3); // X start, Y start in mm
         $pdf->Write(0, strtoupper($official2_name_second_part));
       }
       else {
         $pdf->SetFont('Arial','I',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
         $pdf->SetTextColor(0,0,0); // RGB
         $pdf->SetXY(139,102.3); // X start, Y start in mm
         $pdf->Write(0, strtoupper($official2_name));
       }


       if(strlen($official2_designation) > 25)
       {
         $desig1 = substr($official2_designation,0,25)."-";
         $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
         $pdf->SetTextColor(0,0,0); // RGB
         $pdf->SetXY(139,116.3); // X start, Y start in mm
         $pdf->Write(0, strtoupper($desig1));
         $desig1_name_second_part = substr($official2_designation,25);
         $pdf->SetXY(139,121.3); // X start, Y start in mm
         $pdf->Write(0, strtoupper($desig1_name_second_part));
       }
       else {
         $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
         $pdf->SetTextColor(0,0,0); // RGB
         $pdf->SetXY(139,116.3); // X start, Y start in mm
         $pdf->Write(0, strtoupper($official2_designation));
       }



		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $y=0;
		   $official2_office_name_and_code = $official2_office_name."(".$official2_office_id.")";
		   while(strlen($official2_office_name_and_code) > 23)
			 {
			   $string1 = substr($official2_office_name_and_code,0,23)."-";
			   $official2_office_name_and_code = substr($official2_office_name_and_code,23);
			   $pdf->SetXY(139,130.3+$y); // X start, Y start in mm
			   $pdf->Write(0,$string1);
			   $y += 7;
			 }
		   $pdf->SetXY(139,130.3+$y); // X start, Y start in mm
		   $pdf->Write(0,$official2_office_name_and_code);

       $official2_phone_number="Ph: ".$official2_phone_number;
		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(139,158.3); // X start, Y start in mm
		   $pdf->Write(0, $official2_phone_number);



       if(strlen($official3_name) > 23)
       {
         $string3 = substr($official3_name,0,23)."-";
         $pdf->SetFont('Arial','I',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
         $pdf->SetTextColor(0,0,0); // RGB
         $pdf->SetXY(21,102.3); // X start, Y start in mm
         $pdf->Write(0, strtoupper($string3));
         $official3_name_second_part = substr($official3_name,23);
         $pdf->SetXY(21,107.3); // X start, Y start in mm
         $pdf->Write(0, strtoupper($official3_name_second_part));
       }
       else {
         $pdf->SetFont('Arial','I',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
         $pdf->SetTextColor(0,0,0); // RGB
         $pdf->SetXY(21,102.3); // X start, Y start in mm
         $pdf->Write(0, strtoupper($official3_name));
       }



       if(strlen($official3_designation) > 24)
       {
         $desig3 = substr($official3_designation,0,24)."-";
         $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
         $pdf->SetTextColor(0,0,0); // RGB
         $pdf->SetXY(21,116.3); // X start, Y start in mm
         $pdf->Write(0, strtoupper($desig3));
         $desig3_name_second_part = substr($official3_designation,24);
         $pdf->SetXY(21,121.3); // X start, Y start in mm
         $pdf->Write(0, strtoupper($desig3_name_second_part));
       }
       else {
         $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
         $pdf->SetTextColor(0,0,0); // RGB
         $pdf->SetXY(21,116.3); // X start, Y start in mm
         $pdf->Write(0, strtoupper($official3_designation));
       }




		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB

		   $y=0;
		   $official3_office_name_and_code = $official3_office_name."(".$official3_office_id.")";
		   while(strlen($official3_office_name_and_code) > 23)
			 {
			   $string1 = substr($official3_office_name_and_code,0,23)."-";
			   $official3_office_name_and_code = substr($official3_office_name_and_code,23);
			   $pdf->SetXY(21,130.3+$y); // X start, Y start in mm
			   $pdf->Write(0,$string1);
			   $y += 7;
			 }
		   $pdf->SetXY(21,130.3+$y); // X start, Y start in mm
		   $pdf->Write(0,$official3_office_name_and_code);

		   /*
		   $pdf->SetXY(27,93); // X start, Y start in mm
		   if(strlen($official3_office_name) <= 23)
		   $pdf->Write(0, $official3_office_name);
		   else
			 {
			   $string1 = substr($official3_office_name,0,22)."-";
			   $pdf->Write(0,$string1);
			   $string2 = substr($official3_office_name,22);
			   $pdf->SetXY(27,100); // X start, Y start in mm
			   $pdf->Write(0,$string2);
			 }



		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(27,107); // X start, Y start in mm
		   $pdf->Write(0, $official3_office_id);
		   */
       $official3_phone_number="Ph: ".$official3_phone_number;
		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(21,158); // X start, Y start in mm
		   $pdf->Write(0, $official3_phone_number);
		}
	}

        $pdf->Output($output, "I");

}
?>
