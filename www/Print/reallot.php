<?php
	
	session_start();
	if(!isset($_SESSION['login_user']))
	{
		echo "<script>alert('Session Expired');</script>";
		echo '<script type="text/javascript">
				location.replace("../index.php");
				</script>';
	}
	
	//including connection file
		include "../connection.php";
?>
<html>
<head>
	<title></title>
	<!--including css file-->
	<link rel="stylesheet" type="text/css" href="../css.css">

	<style>
	a
	{
		color: black;
	}
	</style>

</head>
<body>

	<!--including menu file-->
	<?php include "../menu.php"; ?>


	<h2 class="header" style = "Arial,Garamond,Sans-serif;font-size:20px;">ORDER OF THE APPOINTMENT OF POLLING OFFICERS FOR ABSENTEE VOTERS</h3>
<?php

	if(isset($_GET['official1_id']))
	{
		$exemp_official1_id=$_GET['official1_id'];
		$team_id=$_GET['team_id'];
		$category=$_GET['category'];
		//select lac_no
		$result=mysqli_query($conn,"select lac_no from election_official1 where official1_id=$exemp_official1_id");
		$row = mysqli_fetch_array($result);
		$lac=$row['lac_no'];
		// Select data from election_official1
		$details=mysqli_query($conn,"select * from election_official1 where reg_lac_no!='$lac' and reside_lac_no!='$lac' and status='FRESH';");

?>
		<table  class="view_table">
  				<tr>
		<!--
					<th colspan="2">Update</th>
		-->
					<th>Update</th>
					<th>Official ID</th>
					<th>Name</th>
					<th>Office Type ID</th>
					<th>Office Name</th>
					<th>Address 1</th>
					<th>Phone No</th>
					<th>LAC No</th>
					<th>Designation</th>
					<th>Gazetted</th>
					<th>Clazz</th>
					<th>Residence Phone</th>
					<th>Mobile Phone</th>
					<th>Reg LAC No</th>
					<th>Reside LAC No</th>
					<th>Election ID</th>
					<th>Sex</th>
					<th>Retirement Date</th>
					<th>Status</th>

  				</tr>
<?php

		while($data = mysqli_fetch_array($details))
		{
?>
				<tr>
					<td><a href="reallocation.php?id=<?php echo $data['official1_id']; ?>&team_id=<?php echo $team_id ?>&category=<?php echo $category; ?>&exemp_id=<?php echo $exemp_official1_id ?>">Allot</a></td>
    				<td><?php echo $data['official1_id']; ?></td>
					<td><?php echo $data['name']; ?></td>
					<td><?php echo $data['office_type_id']; ?></td>
					<td><?php echo $data['office_name']; ?></td>
					<td><?php echo $data['address1']; ?></td>
					<td><?php echo $data['phone_no']; ?></td>
					<td><?php echo $data['lac_no']; ?></td>
					<td><?php echo $data['designation']; ?></td>
					<td><?php echo $data['gazetted']; ?></td>
					<td><?php echo $data['clazz']; ?></td>
					<td><?php echo $data['residence_phone_no']; ?></td>
					<td><?php echo $data['mobile_phone_no']; ?></td>
					<td><?php echo $data['reg_lac_no']; ?></td>
					<td><?php echo $data['reside_lac_no']; ?></td>
					<td><?php echo $data['election_id']; ?></td>
					<td><?php echo $data['sex']; ?></td>
					<td><?php echo $data['retirement_date']; ?></td>
					<td><?php echo $data['status']; ?></td>


    				</tr>
<?php
		}
?>
			</table>
<?php


	}
	if(isset($_GET['official2_id']))
	{
		$exemp_official2_id=$_GET['official2_id'];
		$team_id=$_GET['team_id'];
		$category=$_GET['category'];
		//select lac_no
		$result=mysqli_query($conn,"select lac_no from election_official2 where official2_id=$exemp_official2_id");
		$row = mysqli_fetch_array($result);
		$lac=$row['lac_no'];
		// Select data from election_official2
		$details=mysqli_query($conn,"select * from election_official2 where reg_lac_no!='$lac' and reside_lac_no!='$lac' and status='FRESH';");

?>
		<table  class="view_table">
  				<tr>
					<th>Update</th>
					<th>Officia2 ID</th>
					<th>Name</th>
					<th>Office Type ID</th>
					<th>Office Name</th>
					<th>Address 1</th>
					<th>Phone No</th>
					<th>LAC No</th>
					<th>Designation</th>
					<th>Gazetted</th>
					<th>Clazz</th>
					<th>Residence Phone</th>
					<th>Mobile Phone</th>
					<th>Reg LAC No</th>
					<th>Reside LAC No</th>
					<th>Election ID</th>
					<th>Sex</th>
					<th>Retirement Date</th>
					<th>Status</th>

  				</tr>
<?php

		while($data = mysqli_fetch_array($details))
		{
?>
				<tr>
					<td><a href="reallocation.php?id=<?php echo $data['official2_id']; ?>&team_id=<?php echo $team_id ?>&category=<?php echo $category; ?>&exemp_id=<?php echo $exemp_official2_id ?>">Allot</a></td>
    				<td><?php echo $data['official2_id']; ?></td>
					<td><?php echo $data['name']; ?></td>
					<td><?php echo $data['office_type_id']; ?></td>
					<td><?php echo $data['office_name']; ?></td>
					<td><?php echo $data['address1']; ?></td>
					<td><?php echo $data['phone_no']; ?></td>
					<td><?php echo $data['lac_no']; ?></td>
					<td><?php echo $data['designation']; ?></td>
					<td><?php echo $data['gazetted']; ?></td>
					<td><?php echo $data['clazz']; ?></td>
					<td><?php echo $data['residence_phone_no']; ?></td>
					<td><?php echo $data['mobile_phone_no']; ?></td>
					<td><?php echo $data['reg_lac_no']; ?></td>
					<td><?php echo $data['reside_lac_no']; ?></td>
					<td><?php echo $data['election_id']; ?></td>
					<td><?php echo $data['sex']; ?></td>
					<td><?php echo $data['retirement_date']; ?></td>
					<td><?php echo $data['status']; ?></td>


    				</tr>
<?php
		}
?>
			</table>
<?php
	}
	if(isset($_GET['observer_id']))
	{
		$exemp_observer_id=$_GET['observer_id'];
		$team_id=$_GET['team_id'];
		$category=$_GET['category'];
		//select lac_no
		$result=mysqli_query($conn,"select lac_no from election_observer where observer_id=$exemp_observer_id");
		$row = mysqli_fetch_array($result);
		$lac=$row['lac_no'];
		// Select data from election_observer
		$details=mysqli_query($conn,"select * from election_observer where reg_lac_no!='$lac' and reside_lac_no!='$lac' and status='FRESH';");

?>
		<table  class="view_table">
  				<tr>
		<!--
					<th colspan="2">Update</th>
		-->
					<th>Update</th>
					<th>Observer ID</th>
					<th>Name</th>
					<th>Office Type ID</th>
					<th>Office Name</th>
					<th>Address 1</th>
					<th>Phone No</th>
					<th>LAC No</th>
					<th>Designation</th>
					<th>Gazetted</th>
					<th>Clazz</th>
					<th>Residence Phone</th>
					<th>Mobile Phone</th>
					<th>Reg LAC No</th>
					<th>Reside LAC No</th>
					<th>Election ID</th>
					<th>Sex</th>
					<th>Retirement Date</th>
					<th>Status</th>

  				</tr>
<?php

		while($data = mysqli_fetch_array($details))
		{
?>
				<tr>
					<td><a href="reallocation.php?id=<?php echo $data['observer_id']; ?>&team_id=<?php echo $team_id ?>&category=<?php echo $category; ?>&exemp_id=<?php echo $exemp_observer_id ?>">Allot</a></td>
    				<td><?php echo $data['observer_id']; ?></td>
					<td><?php echo $data['name']; ?></td>
					<td><?php echo $data['office_type_id']; ?></td>
					<td><?php echo $data['office_name']; ?></td>
					<td><?php echo $data['address1']; ?></td>
					<td><?php echo $data['phone_no']; ?></td>
					<td><?php echo $data['lac_no']; ?></td>
					<td><?php echo $data['designation']; ?></td>
					<td><?php echo $data['gazetted']; ?></td>
					<td><?php echo $data['clazz']; ?></td>
					<td><?php echo $data['residence_phone_no']; ?></td>
					<td><?php echo $data['mobile_phone_no']; ?></td>
					<td><?php echo $data['reg_lac_no']; ?></td>
					<td><?php echo $data['reside_lac_no']; ?></td>
					<td><?php echo $data['election_id']; ?></td>
					<td><?php echo $data['sex']; ?></td>
					<td><?php echo $data['retirement_date']; ?></td>
					<td><?php echo $data['status']; ?></td>


    				</tr>
<?php
		}
?>
			</table>
<?php
	}
?>
<div style="position:relative; bottom:0; width:100%;">
	<?php
	//including footer file
	include "../Footer.php";
	?>
</div>
</body>
</html>
