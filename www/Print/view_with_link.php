<?php
	
	session_start();
	if(!isset($_SESSION['login_user']))
	{
		echo "<script>alert('Session Expired');</script>";
		echo '<script type="text/javascript">
				location.replace("../index.php");
				</script>';
	}
		
	//including connection file
		include "../connection.php";
	
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<!--including css file-->
	<link rel="stylesheet" type="text/css" href="../css.css">
	<style>
	a
	{
		
		color: black;
	}
	</style>
</head>
<body>

	<!--including menu file-->
	<?php include "../menu.php"; ?>


	<h2 class="header" style = "Arial,Garamond,Sans-serif;font-size:20px;">ORDER OF THE APPOINTMENT OF POLLING OFFICERS FOR ABSENTEE VOTERS</h3>

		<div class="form">
		<form id="view_link" name="view_link" method="post" action="">
			<table  class="view_table">
				<tr>
					<th>Team Id</th>
					<th>Micro Observer</th>
					<th>Official 1</th>
					<th>Official 2</th>
					<th>Village</th>
					<!--<th colspan="2"></th>-->
				</tr>
				<?php
					//fetching datas from tables
					$records = mysqli_query($conn,"SELECT team_id,e1.official1_id as official1_id, e1.name as name1,
								e2.official2_id as official2_id, e2.name as name2, village_name,e3.observer_id as observer_id,e3.name as name3 
								FROM election_allotment_details
								INNER JOIN election_official1 e1 ON e1.official1_id = election_allotment_details.official1_id
								INNER JOIN election_official2 e2 ON e2.official2_id = election_allotment_details.official2_id
								INNER JOIN election_village ON election_village.village_code =election_allotment_details.village_code
								INNER JOIN election_observer e3 ON e3.observer_id = election_allotment_details.observer_id");
					while($data = mysqli_fetch_assoc($records))
					{
				?>
						<!--  Name as Link  -->
						<tr>
							<td><?php echo $data['team_id']; ?></td>
							<td><a href="reallot.php?observer_id=<?php echo $data['observer_id']; ?>&team_id=<?php echo $data['team_id']; ?>&category=<?php echo 'observer'; ?>"><?php echo $data['name3']; ?></a></td>
							<td><a href="reallot.php?official1_id=<?php echo $data['official1_id']; ?>&team_id=<?php echo $data['team_id']; ?>&category=<?php echo'official1';?>"><?php echo $data['name1']; ?></a></td>
							<td><a href="reallot.php?official2_id=<?php echo $data['official2_id']; ?>&team_id=<?php echo $data['team_id']; ?>&category=<?php echo'official2';?>"><?php echo $data['name2']; ?></a></td>
							<td><?php echo $data['village_name']; ?></td>
						</tr>

				<?php
					}
				?>
			</table>
		</form>
		</div>
	
<div style="position:relative; bottom:0; width:100%;">
	<?php
	//including footer file
	include "../Footer.php";
	?>
</div>

</body>
</html>
