<?php
session_start();
if(!isset($_SESSION['login_user']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}

//including connection file
    include "../connection.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<!--
	<style media="screen">
				.data
					{
						border:2px solid black;
						border-collapse:collapse;
						margin: auto;
						 width: 320px;
						padding: 10px;
					}
	</style>-->
	<!--including css file-->
	<link rel="stylesheet" type="text/css" href="../css.css">
</head>
<body>
<?php include "../menu.php"; ?>
		<h1 class="header" style = "Arial,Garamond,Sans-serif;font-size:20px;">General Election to KLA 2021 – Appointment of Polling Officers
 of Absentee Voters – Posting Order Serving – Reg</h1>


		<form id="section_view" name="section_view" method="post" action="printing_institution.php">
		<!--	<div class="data">
				<table>
					<tr>

						<td><label for="dates">Date of Issue :</label></td>
						<td><input type="date" name="date" id="dates" value="" size="4"></td>
					</tr>

				</table>
			</div> <br> -->
			<div class="form">
<?php
	$office=mysqli_query($conn,"select DISTINCT office_name from ((select  * from election_official1 where status='Allotted')UNION(select  * from election_official2 where status='Allotted')UNION(select  * from election_observer where status='Allotted'))as p");

    while($data = mysqli_fetch_assoc($office))
	{
    $count=1;
    ?>
    <table  class="view_table">
              <tr><td colspan="5" align="center"><b><?php echo $data['office_name'];  ?></b></td><tr>
        <tr>
                  <th>Sl No</th>
					        <th>Name</th>
					        <th>Designation</th>
					        <th>Category</th>
                  <th>Signature</th>
        <!--<th colspan="2"></th>-->
        </tr>
        <?php
					$sql3=mysqli_query($conn,"Drop view if exists view_ids");
					$sql5=mysqli_query($conn,"CREATE VIEW view_ids AS (SELECT CONCAT('off1',official1_id) as ID,p1.name,p1.designation,p1.office_name from election_official1 as p1 WHERE p1.status='Allotted')
					 UNION
					  (SELECT CONCAT( 'off2',official2_id) as ID ,p2.name,p2.designation,p2.office_name from election_official2 as p2 WHERE p2.status='Allotted')
						UNION
					 (SELECT CONCAT('obse',observer_id) as ID ,p3.name,p3.designation,p3.office_name from election_observer as p3 WHERE p3.status='Allotted')");



        $details=mysqli_query($conn,"select * from view_ids");

      while ($row = mysqli_fetch_assoc($details))
      {
				  $id=$row['ID'];





				$x=substr($id,0,4);




				if($x=="off1")
					$category='Official 1';
				if($x=="off2")
					$category='Official 2';
				if($x=="obse")
							$category='Micro Observer';



        if($row['office_name']==$data['office_name'])
        {
          ?>
          <tr>
              <td><?php echo $count; ?></td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['designation']; ?></td>

            <td><?php echo $category ?></td>
          </tr>
        <?php
        $count++;
        }

      }
      ?>
    </table> <br><br>
      <?php
}
 ?>


      <p align="center">
      <button type="submit" name="submit" class="submit" style="width:200px">Print</button></p>
				</div>
		</form>


	<div style="position:relative; bottom:0; width:100%;">
<?php
//including footer file
include "../Footer.php";
?>
</body>
</html>
