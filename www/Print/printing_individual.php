<?php
include '../connection.php';
require_once('../FPDF/fpdf.php');
require_once('../FPDI/src/autoload.php');
use \setasign\Fpdi\Fpdi;


$records = mysqli_query($conn,"SELECT team_id,e1.name as name1,e1.designation as designation1,e1.office_code_no as office_code_no_1 ,e1.office_name as office_name_1,e1.mobile_phone_no as mob1,
e2.name as name2,e2.designation as designation2,e2.office_code_no as office_code_no_2,e2.office_name as office_name_2,e2.mobile_phone_no as mob2 ,
e3.name as name3,e3.designation as designation3,e3.office_code_no as office_code_no_3 ,e3.office_name as office_name_3,e3.mobile_phone_no as mob3 FROM election_allotment_details
 INNER JOIN election_official1 e1 ON e1.official1_id = election_allotment_details.official1_id
 INNER JOIN election_official2 e2 ON e2.official2_id = election_allotment_details.official2_id
 INNER JOIN election_observer e3 ON e3.observer_id = election_allotment_details.observer_id");

 /*$date=date('d/m/Y',strtotime($_POST['date']));
 $time=$_POST['time'];
 $place=$_POST['place'];*/
generatePDF("appointment.pdf", "Appointment.pdf",$records,$conn);


 function generatePDF($source, $output,$records,$conn)
 {

$pdf = new FPDI(); // 'Landscape','mm',array(210,148)
while($data = mysqli_fetch_assoc($records))
{
  $team_id=$data['team_id'];

  $team_fetch=mysqli_query($conn,"select * from election_village where village_code=
  (select village_code from election_allotment_details where team_id=$team_id)");
  $lac_fetch=mysqli_fetch_assoc($team_fetch);
  $allotted_lac=$lac_fetch['lac_number'];

  $Lac_name_fetch=mysqli_query($conn,"select * from election_nearbylac where lac_number='$allotted_lac'");
  $lac_names=mysqli_fetch_assoc($Lac_name_fetch);
  $allotted_lac_name=$lac_names['lac_name'];

            $official1_name=$data['name1'];
            $official1_designation=$data['designation1'];
            $official1_phone_number=$data['mob1'];
            $official1_office_id=$data['office_code_no_1'];
            $official1_office_name=$data['office_name_1'];

            $official2_name=$data['name2'];
            $official2_designation=$data['designation2'];
            $official2_phone_number=$data['mob2'];
            $official2_office_id=$data['office_code_no_2'];
            $official2_office_name=$data['office_name_2'];

            $official3_name=$data['name3'];
            $official3_designation=$data['designation3'];
            $official3_phone_number=$data['mob3'];
            $official3_office_id=$data['office_code_no_3'];
            $official3_office_name=$data['office_name_3'];

$pdf->AddPage(); //Add A4 by Default
$pagecount = $pdf->setSourceFile($source);
$tppl = $pdf->importPage(1);

$pdf->useTemplate($tppl, 0, 0); //need to be set , 210, 148
/*
$pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
$pdf->SetTextColor(0,0,0); // RGB
$pdf->SetXY(68.6,160.6); // X start, Y start in mm
$pdf->Write(0, $time);

$pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
$pdf->SetTextColor(0,0,0); // RGB
$pdf->SetXY(68.6,165.4); // X start, Y start in mm
$pdf->Write(0, $place);

$pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
$pdf->SetTextColor(0,0,0); // RGB
$pdf->SetXY(68.6,155.8); // X start, Y start in mm
$pdf->Write(0, $date); */


		   $pdf->SetFont('Arial','I',12); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(178.9,175.2); // X start, Y start in mm
		   $pdf->Write(0, $team_id);

		   $allotted_lac=$allotted_lac." - ".$allotted_lac_name;
		   $pdf->SetFont('Arial','I',12); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(90.2,175.2); // X start, Y start in mm
		   $pdf->Write(0, $allotted_lac);
       /*
		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(96.2,168.4); // X start, Y start in mm
		   $pdf->Write(0, $allotted_lac_name);*/
       /*
		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(65.6,150.5); // X start, Y start in mm
		   $pdf->Write(0, $place);

		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(65.6,145.5); // X start, Y start in mm
		   $pdf->Write(0, $time);

		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(65.6,140.5); // X start, Y start in mm
		   $pdf->Write(0, $date);
       */
		   $pdf->SetFont('Arial','I',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(80.1,99.3); // X start, Y start in mm
		   $pdf->Write(0, $official1_name);

		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(80.1,113.3); // X start, Y start in mm
		   $pdf->Write(0, $official1_designation);

		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $y=0;
		   $official1_office_name_and_code = $official1_office_name."(".$official1_office_id.")";
		   while(strlen($official1_office_name_and_code) > 25)
			 {
			   $string1 = substr($official1_office_name_and_code,0,25)."-";
			   $official1_office_name_and_code = substr($official1_office_name_and_code,25);
			   $pdf->SetXY(80.1,127.3+$y); // X start, Y start in mm
			   $pdf->Write(0,$string1);
			   $y += 8;
			 }
		   $pdf->SetXY(80.1,127.3+$y); // X start, Y start in mm
		   $pdf->Write(0,$official1_office_name_and_code);

		   /*
		   if(strlen($official1_office_name) <= 25)
		   $pdf->Write(0, $official1_office_name);
		   else
			 {
			   $string1 = substr($official1_office_name,0,25)."-";
			   $pdf->Write(0,$string1);
			   $string2 = substr($official1_office_name,25);
			   $pdf->SetXY(78,100); // X start, Y start in mm
			   $pdf->Write(0,$string2);
			 }


		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(78,107); // X start, Y start in mm
		   $pdf->Write(0, $official1_office_id);
		   */
       $official1_phone_number="Ph:".$official1_phone_number;
		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(80.1,149.3); // X start, Y start in mm
		   $pdf->Write(0, $official1_phone_number);

		   $pdf->SetFont('Arial','I',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(139,99.3); // X start, Y start in mm
		   $pdf->Write(0, $official2_name);

		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(139,113.3); // X start, Y start in mm
		   $pdf->Write(0, $official2_designation);


		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $y=0;
		   $official2_office_name_and_code = $official2_office_name."(".$official2_office_id.")";
		   while(strlen($official2_office_name_and_code) > 23)
			 {
			   $string1 = substr($official2_office_name_and_code,0,23)."-";
			   $official2_office_name_and_code = substr($official2_office_name_and_code,23);
			   $pdf->SetXY(139,127.3+$y); // X start, Y start in mm
			   $pdf->Write(0,$string1);
			   $y += 7;
			 }
		   $pdf->SetXY(139,127.3+$y); // X start, Y start in mm
		   $pdf->Write(0,$official2_office_name_and_code);

		   /*
		   $pdf->SetXY(135,93); // X start, Y start in mm
		   if(strlen($official2_office_name) <= 25)
		   $pdf->Write(0, $official2_office_name);
		   else
			 {
			   $string1 = substr($official2_office_name,0,25)."-";
			   $pdf->Write(0,$string1);
			   $string2 = substr($official2_office_name,25);
			   $pdf->SetXY(135,100); // X start, Y start in mm
			   $pdf->Write(0,$string2);
			 }




		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(135,107); // X start, Y start in mm
		   $pdf->Write(0, $official2_office_id);
		   */
       $official2_phone_number="Ph:".$official2_phone_number;
		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(139,149.3); // X start, Y start in mm
		   $pdf->Write(0, $official2_phone_number);

		   $pdf->SetFont('Arial','I',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(22,99.3); // X start, Y start in mm
		   $pdf->Write(0, $official3_name);

		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(22,113.3); // X start, Y start in mm
		   $pdf->Write(0, $official3_designation);


		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB

		   $y=0;
		   $official3_office_name_and_code = $official3_office_name."(".$official3_office_id.")";
		   while(strlen($official3_office_name_and_code) > 23)
			 {
			   $string1 = substr($official3_office_name_and_code,0,23)."-";
			   $official3_office_name_and_code = substr($official3_office_name_and_code,23);
			   $pdf->SetXY(22,127.3+$y); // X start, Y start in mm
			   $pdf->Write(0,$string1);
			   $y += 7;
			 }
		   $pdf->SetXY(22,127.3+$y); // X start, Y start in mm
		   $pdf->Write(0,$official3_office_name_and_code);

		   /*
		   $pdf->SetXY(27,93); // X start, Y start in mm
		   if(strlen($official3_office_name) <= 23)
		   $pdf->Write(0, $official3_office_name);
		   else
			 {
			   $string1 = substr($official3_office_name,0,22)."-";
			   $pdf->Write(0,$string1);
			   $string2 = substr($official3_office_name,22);
			   $pdf->SetXY(27,100); // X start, Y start in mm
			   $pdf->Write(0,$string2);
			 }



		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(27,107); // X start, Y start in mm
		   $pdf->Write(0, $official3_office_id);
		   */
       $official3_phone_number="Ph:".$official3_phone_number;
		   $pdf->SetFont('Arial','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		   $pdf->SetTextColor(0,0,0); // RGB
		   $pdf->SetXY(22,149.3); // X start, Y start in mm
		   $pdf->Write(0, $official3_phone_number);
}
$pdf->Output($output, "I");
}
?>
