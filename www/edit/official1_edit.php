<?php 
session_start();
if(!isset($_SESSION['login_user']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}

//Including database connection file
include "../connection.php" ; 
?>
<!DOCTYPE html>
<html>
<head>
  <title>Official 1 Edit</title>
  <!--including css file-->
	<style>
		* 
		{
		box-sizing: border-box;
		}
		<!--style the body-->
		body 
		{
		font-family: Arial, Helvetica, sans-serif;
		margin: auto;
		}

		<!-- Style the header -->
		.header 
		{
		padding: 30px;
		text-align: center;
		background: white;
		color: black;
		}

		<!-- Increase the font size of the h1 element -->
		.header h1 
		{
		font-size: 40px;
		}

		<!-- Style the top navigation bar -->
		.navbar 
		{
		overflow: hidden;
		background-color: #333;
		}

		<!-- Style the navigation bar links -->
		.navbar a 
		{
		float: left;
		display: block;
		color: white;
		text-align: center;
		padding: 14px 20px;
		text-decoration: none;
		}

		<!-- Right-aligned link -->
		.navbar a.right 
		{
		float: right;
		}

		<!-- Change color on hover -->
		.navbar a:hover 
		{
		background-color: #ddd;
		color: black;
		}
		<!--style the div with class name form-->
		.form 
		{
		  margin: auto;
		  width: 60%;
		  border: 3px solid #555555;<!-- Black -->
		  padding: 10px;
		}
		table 
		{
		  width: 100%;
		}
		.buttontd 
		{
		text-align: center;
		}
		span 
		{
		color:#FF0000;
		}
		input[type=text], input[type=tel],input[type=date],input[type=email], select,textarea 
		{
		  width: 100%;
		  padding: 12px 20px;
		  margin: 8px 0;
		  display: inline-block;
		  border: 1px solid #ccc;
		  border-radius: 4px;
		  box-sizing: border-box;
		}
		select 
		{
		  width: 100%;
		  border: none;
		  border-radius: 4px;
		  background-color: #f1f1f1;
		}
		<!--style the link with class name btn-->

		.submit 
		{
		background-color: #4CAF50; <!-- Green -->
		color: white;
		}
		button
		{
		  border: none;
		  padding: 10px 15px;
		  text-align: center;
		  text-decoration: none;
		  display: inline-block;
		  transition: 0.3s;
		}

		button:hover 
		{
		  background-color: #3e8e41;
		  color: white;
		}

		.view_table th
		{
			background-color:#999999;
			text-align:left;
			padding:8px;
			font-size:18px;
			color:#000000;
		}
		.view_table td
		{
			text-align:left;
			padding:8px;
		}
		.view_table tr:hover{ background-color: #CCCCCC;}
		.view_table td:hover{ background-color: #3e8e41;color: white;}

</style>

</head>
<body>

	<!--including menu file-->
	<?php include "../menu.php"; ?>
	
<br><br>
	<!--section view form-->
<div class="form">
		<form id="section_view" name="section_view" method="post" action="">
			<table  class="view_table">
  				<tr>
		<!--		
					<th colspan="2">Update</th>
		-->			
					<th>Update</th>
					<th>Official ID</th>
					<th>Name</th>
					<th>Village Code</th>
					<th>District No</th>
					<th>Office Type ID</th>
					<th>Office Name</th>
					<th>Address 1</th>
					<th>Address 2</th>
					<th>Address 3</th>
					<th>Phone No</th>
					<th>LAC No</th>
					<th>Office Code</th>
					<th>Designation</th>
					<th>Pen</th>
					<th>Gazetted</th>
					<th>Clazz</th>
					<th>Residence Phone</th>
					<th>Mobile Phone</th>
					<th>Reg LAC No</th>
					<th>Reside LAC No</th>
					<th>Election ID</th>
					<th>Sex</th>
					<th>Retirement Date</th>
					<th>Status</th>
					
  				</tr>
<?php
	//fetching datas from table election_official1
	$records = mysqli_query($conn,"select * from election_official1"); 

	while($data = mysqli_fetch_array($records))
	{
?>
				<tr>
					<td><a href="official1_edit1.php?official1_id=<?php echo $data['official1_id']; ?>">Edit</a></td>
    	<!--			<td><a href="official1_edit2.php?official1_id=<?php echo $data['official1_id']; ?>" onClick="return confirm('Are you sure to delete ?');">Delete</a>
		-->		
    				<td><?php echo $data['official1_id']; ?></td>
					<td><?php echo $data['name']; ?></td>
					<td><?php echo $data['village_code']; ?></td> 
					<td><?php echo $data['dist_no']; ?></td>
					<td><?php echo $data['office_type_id']; ?></td> 
					<td><?php echo $data['office_name']; ?></td> 
					<td><?php echo $data['address1']; ?></td>
					<td><?php echo $data['address2']; ?></td>
					<td><?php echo $data['address3']; ?></td> 
					<td><?php echo $data['phone_no']; ?></td>
					<td><?php echo $data['lac_no']; ?></td>
					<td><?php echo $data['office_code_no']; ?></td>  
					<td><?php echo $data['designation']; ?></td> 
					<td><?php echo $data['pen']; ?></td>
					<td><?php echo $data['gazetted']; ?></td>
					<td><?php echo $data['clazz']; ?></td> 
					<td><?php echo $data['residence_phone_no']; ?></td>
					<td><?php echo $data['mobile_phone_no']; ?></td> 
					<td><?php echo $data['reg_lac_no']; ?></td>
					<td><?php echo $data['reside_lac_no']; ?></td> 
					<td><?php echo $data['election_id']; ?></td>
					<td><?php echo $data['sex']; ?></td> 
					<td><?php echo $data['retirement_date']; ?></td>
					<td><?php echo $data['status']; ?></td> 
					
					
    				</tr>	
<?php
}
?>
			</table>
		</form>
	</div>
	<?php
		//including footer file
		include "../Footer.php";
	?>
</body>
</html>