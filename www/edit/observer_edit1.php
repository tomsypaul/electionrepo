<?php 
session_start();
if(!isset($_SESSION['login_user']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}
//including database connection file
include "../connection.php" ; 

$observer_id=$_GET['observer_id'];
//fetching data from table election_observer
$qry="SELECT * FROM election_observer WHERE observer_id=$observer_id";
$result=$conn->query($qry);
if($result->num_rows>0)
{
	while($data=$result->fetch_assoc())
	{

?>
<!DOCTYPE html>
<html>
<head>
<title>Admin_Section_Edit</title>
<link rel="stylesheet" href="../ktfo_css.css">
</head>
<body>
<script>
	<!--confirmation message before update-->
	function update()
	{
		return confirm("Do you really want to update");
	}
</script>

	<!--including menu file-->
	<?php include "../menu.php"; ?>
	
<br><br>
<div class="form">
<form id="observer_edit" name="observer_edit" method="post" action="" onSubmit="return update()">
  <table>
  	<tr>
      <td>Official ID</td>
      <td><input type="text" name="observer_id" disabled="disabled" value="<?php echo $data['observer_id'];?>"/></td>
    </tr>
	<tr>
      <td>Name</td>
      <td><input type="text" name="name" id="name" value="<?php echo $data['name'];?>" /></td>
    </tr>
	<tr>
      <td>Village Code</td>
      <td><input type="text" id="village_code" name="village_code" value="<?php echo $data['village_code'];?>" /></td>
    </tr>
	<tr>
	  <td>District No</td>
      <td><input type="text" id="dist_no" name="dist_no" value="<?php echo $data['dist_no'];?>"/></td>
    </tr>
	<tr>
      <td>Office Type ID</td>
      <td><input type="text" name="office_type_id" id="office_type_id" value="<?php echo $data['office_type_id'];?>" /></td>
    </tr>
	<tr>
      <td>Official Name</td>
      <td><input type="text" name="office_name" id="office_name" value="<?php echo $data['office_name'];?>"/></td>
    </tr>
	<tr>
      <td>Address 1</td>
      <td><input type="text" name="address1" id="address1" value="<?php echo $data['address1'];?>" /></td>
    </tr>
	<tr>
      <td>Address 2</td>
      <td><input type="text" id="address2" name="address2" value="<?php echo $data['address2'];?>" /></td>
    </tr>
	<tr>
	  <td>Address 3</td>
      <td><input type="text" id="address3" name="address3" value="<?php echo $data['address3'];?>"/></td>
    </tr>
	<tr>
      <td> Phone No</td>
      <td><input type="text" name="phone_no" value="<?php echo $data['phone_no'];?>"/></td>
    </tr>
	<tr>
      <td>LAC No</td>
      <td><input type="text" name="lac_no"  value="<?php echo $data['lac_no'];?>"/></td>
    </tr>
	<tr>
      <td>Office Code</td>
      <td><input type="text" name="office_code_no" value="<?php echo $data['office_code_no'];?>"  /></td>
    </tr>
	<tr>
      <td>Designation</td>
      <td><input type="text" id="designation" name="designation" value="<?php echo $data['designation'];?>" /></td>
    </tr>
	<tr>
	  <td>Pen</td>
      <td><input type="text" id="pen" name="pen" value="<?php echo $data['pen'];?>"/></td>
    </tr>
	<tr>
      <td>Gazetted</td>
      <td><input type="text" name="gazetted" value="<?php echo $data['gazetted']; ?>"></td>
    </tr>
	<tr>
      <td>Clazz</td>
      <td><input type="text" id="clazz" name="clazz" value="<?php echo $data['clazz'];?>" /></td>
    </tr>
	<tr>
	  <td>Residence Phone</td>
      <td><input type="text" id="residence_phone_no" name="residence_phone_no" value="<?php echo $data['residence_phone_no'];?>"/></td>
    </tr>
	<tr>
	  <td>Mobile Phone</td>
      <td><input type="text" id="mobile_phone_no" name="mobile_phone_no" value="<?php echo $data['mobile_phone_no'];?>"/></td>
    </tr>
	<tr>
      <td>Regist LAC No</td>
      <td><input type="text" name="reg_lac_no" id="reg_lac_no" value="<?php echo $data['reg_lac_no']; ?>"/></td>
    </tr>
	<tr>
      <td>Reside LAC No</td>
      <td><input type="text" name="reside_lac_no" id="reside_lac_no" value="<?php echo $data['reside_lac_no'];?>"/></td>
    </tr>
	<tr>
      <td>Election ID</td>
      <td><input type="text" name="election_id" id="election_id"value="<?php echo $data['election_id'];?>"/></td>
    </tr>
	<tr>
      <td>Sex</td>
      <td><input type="text" id="sex" name="sex" value="<?php echo $data['sex'];?>" /></td>
    </tr>
	<tr>
	  <td>Retirement Date</td>
      <td><input type="text" id="retirement_date" name="retirement_date" value="<?php echo $data['retirement_date'];?>"/></td>
    </tr>
	<tr>
      <td>Status</td>
      <td><input type="text" name="status" id="status" value="<?php echo $data['status'];?>" readonly="readonly"/></td>
    </tr>
	
    <tr>
	<td colspan="2" align="center"><button type="reset" name="cancel" onClick="window.location='observer_edit.php';return false;">CANCEL</button>
	  	 <button type="submit" name="edit">UPDATE</button></td>
    </tr>
  </table>
</form>
</div>

<div style=" bottom:0; width:100%;">
<?php
//including footer file
include "../Footer.php";

?> 
</div>
</body>
</html>
<?php
 if(isset($_POST['edit']))
 {
	 
 	//$observer_id=$_POST['observer_id'];
	
	$name=$_POST['name'];
	
	$village_code=$_POST['village_code'];
	
	$dist_no=$_POST['dist_no'];
	
	$office_type_id=$_POST['office_type_id'];
	
	$office_name=$_POST['office_name'];
	
	$address1=$_POST['address1'];
	
	$address2=$_POST['address2'];
	
	$address3=$_POST['address3'];
	
	$phone_no=$_POST['phone_no'];
	
	$lac_no=$_POST['lac_no'];
	
	$office_code_no=$_POST['office_code_no'];
	
	$designation=$_POST['designation'];
	
	$pen=$_POST['pen'];
	
	$gazetted=$_POST['gazetted'];
	
	$clazz=$_POST['clazz'];
	
	$residence_phone_no=$_POST['residence_phone_no'];
	
	$mobile_phone_no=$_POST['mobile_phone_no'];
	
	$reg_lac_no=$_POST['reg_lac_no'];
	
	$reside_lac_no=$_POST['reside_lac_no'];
	
	$election_id=$_POST['election_id'];
	
	$sex=$_POST['sex'];
	
	$retirement_date=$_POST['retirement_date'];
	
	$status=$_POST['status'];
	
	//updating election_observer 
	$sql="update election_observer set name='$name' , village_code=$village_code, dist_no=$dist_no, office_type_id=$office_type_id,
	office_name='$office_name',address1='$address1', address2='$address2', address3='$address3',phone_no=$phone_no , lac_no='$lac_no',
	office_code_no=$office_code_no,designation='$designation',pen=$pen , gazetted=$gazetted, clazz=$clazz, residence_phone_no='$residence_phone_no',
	mobile_phone_no=$mobile_phone_no,reg_lac_no='$reg_lac_no',reside_lac_no='$reside_lac_no' , election_id='$election_id', sex='$sex', retirement_date=$retirement_date,
	status='$status' where observer_id=$observer_id";
	
	if($conn->query($sql)== TRUE)
	 	{ 
	?>
	<script>confirm(" Updated Successfully");</script> 
	<?php	
		echo '<script type="text/javascript">
        location.replace("observer_edit.php");
        </script>';  
		} 
	else
		{
	?>
  	<script> alert("failed");</script>  
<?php
		}
}
}
}
?>