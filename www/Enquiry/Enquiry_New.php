<?php 
session_start();
if(!isset($_SESSION['login_user']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}
//Including database connection file
include "../connection.php" ; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>New Equiry</title>
<!--styles file including-->
<link rel="stylesheet" href="../ktfo_css.css">
</head>
<body>
<!--Header-->
<div class="header" align="center">
   <img class="site_logo" height="100" id="logo" src="../gvt.jpg" alt="Kerala logo" >
   <h1>KOTTAYAM TALUK FRONT OFFICE</h1>
</div>
<!--navigation bar -->
   <div class="navbar">
      <a href="../Admin_Home.php">Home</a>
      <a href="../Admin_Application_New/Admin_Application_New_Form.php">New Application</a>
      <a href="../Admin_Pass/Admin_Pass_Generation.php">Issue Pass</a>
   </div>
   <br><br>
<!--form to add new section-->
   <h1 align="center">New Enquiry</h1>
   <div class="form" >
      <form id="enquiry_add" name="enquiry_add" method="post" action="" onSubmit="return submit_form()">
         <table>
	         <tr>
	            <td><label for="name">Name<span> * </span></label></td>
               <td><input type="text" id="name" name="name" required pattern="[A-Za-z ]+" title="Your name is not valid. Only characters A-Z, a-z, space are acceptable"></td>
            </tr>
	         <tr>
	            <td><label for="phone">Phone Number<span> * </span></label></td>
               <td><input type="text" id="phone" name="phone" value="" pattern="[0-9]{10}" required></td>
            </tr>
	         <tr>
               <td><label for="village">Village</label></td>
               <td>
                  <select name="village" id="village">
					      <option value="">Select</option>
					      <option value="Aimanam">Aimanam</option>
                     <option value="Akalakunnam">Akalakunnam</option>
                     <option value="Anikkad">Anikkad</option>
                     <option value="Arpookara">Arpookara</option>
                     <option value="Athirampuzha">Athirampuzha</option>
                     <option value="Ayarkunnam">Ayarkunnam</option>
                     <option value="Chengalam East">Chengalam East</option>
                     <option value="Chengalam South">Chengalam South</option>
                     <option value="Ettumanoor">Ettumanoor</option>
                     <option value="Kaipuzha">Kaipuzha</option>
                     <option value="Kooroppada">Kooroppada</option>
                     <option value="Kottayam">Kottayam</option>
                     <option value="Kumarakam">Kumarakam</option>
                     <option value="Manarcad">Manarcad</option>
                     <option value="Meenadam">Meenadam</option>
                     <option value="Muttampalam">Muttampalam</option>
                     <option value="Nattakam">Nattakam</option>
                     <option value="Onamthuruth">Onamthuruth</option>
                     <option value="Pampady">Pampady</option>
                     <option value="Panachikad">Panachikad</option>
                     <option value="Peroor">Peroor</option>
                     <option value="Perumbaikad">Perumbaikad</option>
                     <option value="Puthuppally">Puthuppally</option>
                     <option value="Thiruvarupu">Thiruvarupu</option>
                     <option value="Veloor">Velloor</option>
                     <option value="Vijayapuram">Vijayapuram</option>
	               </select>
               </td>
            </tr>
	         <tr>
	            <td><label>Application Subject</label></td>
               <td>
                 <input type="text" list="sub" name="subject" id="subject">
                 <datalist id="sub">
                     <option value="Area Alteration">
                     <option value="Arms, Explosive/ Mining">
                     <option value="Auction">
                     <option value="Birth/ Death Registration">
                     <option value="Census">
                     <option value="Certificate">
                     <option value="Chief Ministers DRF">                     
                     <option value="Compassionate Employment">
                     <option value="Covid-19">
                     <option value="Disaster Management">
                     <option value="Election">
                     <option value="Electricity">
                     <option value="Encroachment of Land">
                     <option value="Establishment">
                     <option value="Eviction">
                     <option value="Irrigation">
                     <option value="KLU (Land Utilization)">
                     <option value="Land Assignment">
                     <option value="Land Conversion">
                     <option value="Land Transfer">
                     <option value="Law and Order">
                     <option value="Legal Heirship Certificate">
                     <option value="Luxury Tax">
                     <option value="Miscellanious">
                     <option value="NFBS">  
                     <option value="Paddy and Wetland Act">
                     <option value="Pattayam">
                     <option value="Pensions">
                     <option value="Point Out of Boundary">
                     <option value="Pumping Subsidy">
                     <option value="PV">
                     <option value="PV Appeal">
                     <option value="PV Cancellation">
                     <option value="Resurvey Correction">
                     <option value="Revenue Recovery">
                     <option value="Right to Information Act">
                     <option value="Right to Service">
                     <option value="Surplus Land">
                     <option value="Tree Cutting">    
                 </datalist>
               </td>
            </tr>
            <tr>
               <td>
                  <label for="section">Section</label>
               </td>
               <td>
                  <?php
                     $sql="SELECT section_id, section_name from ktfo_section where for_application='Y' order by section_name;";
                     $result=$conn->query($sql);
                  ?>    
                  <select id="section" name="section">
                     <option  value="" selected>Select</option>
                  <?php                            
                     if($result->num_rows>0)
                        while($row=$result->fetch_assoc())
                           echo '<option value="'.$row['section_id'].'">'.$row['section_name'].'</option>';                    
                  ?> 
                  </select>   
               </td>
              </tr>	
	         <tr>
	            <td><label for="application_no">Application Number</label></td>
               <td><input type="text" id="application_no" name="application_no" value=""></td>
            </tr>	
	         <tr>
	            <td><label for="file">File Number</label></td>
               <td><input type="text" id="file_no" name="file_no" value=""></td>
            </tr>	
	         <tr>
	            <td><label for="date_of_application">Date of Application</label></td>
               <td><input type="date" id="date_of_application" name="date_of_application" value=""></td>
            </tr>
            <tr>
               <td colspan="2" align="center">
                  <button type="reset">RESET</button>
	  	            <button type="submit" name="add" class="submit">SUBMIT</button>
               </td>
            </tr>
         </table>
      </form>
   </div> 
   <?php
		//including footer file
		include "../Footer.php";
	?>
</body>
</html>
<?php
   if(isset($_POST['add']))
   {
 	   $name=$_POST['name'];
	   $phone=$_POST['phone'];
	   $village=$_POST['village'];
	   $subject=$_POST['subject'];
      $section=$_POST['section'];
	   $application_no=$_POST['application_no'];
	   $file_no=$_POST['file_no'];
	   $date_of_application=$_POST['date_of_application'];	
      date_default_timezone_set("Asia/Kolkata");
      $date=date("Y-m-d h:i:s",time());

	//insert new section to table ktfo_enquiry
      if ($section=="") 
         $sql="INSERT INTO ktfo_enquiry(name, phone_number, village, subject, application_number, file_number, date_application, status, date_enquiry)values('$name','$phone','$village','$subject','$application_no','$file_no','$date_of_application','Pending', '$date');"; 
      else
         $sql="INSERT INTO ktfo_enquiry(name, phone_number, village, subject, section_id, application_number, file_number, date_application, status, date_enquiry)values('$name','$phone','$village','$subject',$section,'$application_no','$file_no','$date_of_application','Pending', '$date');";   	
	   if($conn->query($sql))
	   { 
?>
	      <script>
            alert(" Added Successfully");
            location.replace("Enquiry_View.php");
         </script> 
<?php      
	   }
	   else
	   {
?>
  	      <script>alert("failed");</script>  
<?php
	   }
   }
?>
