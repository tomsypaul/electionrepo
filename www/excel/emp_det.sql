-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 01, 2019 at 12:26 PM
-- Server version: 5.5.45
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `employee`
--

-- --------------------------------------------------------

--
-- Table structure for table `emp_det`
--

CREATE TABLE IF NOT EXISTS `emp_det` (
  `empno` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `department` varchar(40) NOT NULL,
  `designation` varchar(20) NOT NULL,
  `email` varchar(60) NOT NULL,
  `phno` varchar(50) NOT NULL,
  PRIMARY KEY (`empno`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `emp_det`
--

INSERT INTO `emp_det` (`empno`, `name`, `department`, `designation`, `email`, `phno`) VALUES
(6, 'Sonupriya P S', 'MCA', 'ASST. PROF', 'sonupriyaps1@gmail.com', '8547460997'),
(7, 'Anitta Antony', 'MCA', 'ASST. PROF', 'anittaantony@gmail.com', '6788888888');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
