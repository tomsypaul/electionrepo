<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width,initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <link rel="stylesheet" href="bootstrap.css">
   <link rel="stylesheet" href="main.css">
   <style type="text/css">
      button
      {
         border-radius: 10px;
      }
      button:hover
      {
         background-color: limegreen;
         color: white;
      }
   </style>   
</head>
<body>
   <center>
      <h1>Your application status...</h1>
   </center>
   <center>
      <table style="width:50%">
         <?php
         include "../connection.php";
         if(isset($_GET['submit']))
         {
            $application_number=$_GET['application_number'];
            $query="SELECT * FROM ktfo_application WHERE application_number='$application_number'";
            $query1="SELECT name FROM ktfo_person WHERE person_id=(SELECT person_id FROM ktfo_application WHERE application_number='$application_number')";
            $query2="SELECT section_name FROM ktfo_section WHERE section_id=(SELECT section_submitted FROM ktfo_application WHERE application_number='$application_number')";
            $query3="SELECT section_name FROM ktfo_section WHERE section_id=(SELECT section_current FROM ktfo_application WHERE application_number='$application_number')";
            $query_run=mysqli_query($conn,$query);
            $query1_run=mysqli_query($conn,$query1);
            $query2_run=mysqli_query($conn,$query2);
            $query3_run=mysqli_query($conn,$query3);
            while($row1=mysqli_fetch_array($query_run))
            {
            ?>
               <tr>
                  <th><i>Application Number:</i></th>
                  <td><?php echo $row1['application_number']?><br></td>
               </tr>
            <?php
               while($row=mysqli_fetch_array($query1_run))
               {
            ?>
                  <tr>
                     <th><i>Name of Applicant:</i></th>
                     <td><?php echo $row['name']?><br></td>
                  </tr>
               <?php   
               }
               ?>
               <tr>
                  <th><i>Application Subject:</i></th>
                  <td><?php echo $row1['application_subject']?><br></td>
               </tr>
               <tr>
                  <th><i>Applied date:</i></th>
                  <td><?php echo date('d-m-Y',strtotime($row1['date_applied'])) ?><br></td>
               </tr>
               <tr>
                  <th><i>Status</i></th>
                  <td><?php echo $row1['status']?><br></td>
               </tr>
            <?php
            }
            while($row=mysqli_fetch_array($query2_run))
            {
            ?>
               <tr>
                  <th><i>Section submitted:</i></th>
                  <td><?php echo $row['section_name']?><br></td>          
               </tr>
            <?php
            }
         }
         ?>
      </table>
   </center>
   <br> 
   <center>
      <div style="text-align: center; width: 50%;">
         <form method="POST">
            <button type="submit" name="back">TRACK ANOTHER</button>
         </form>
      </div>
   </center>  
   <div style="position:fixed; bottom:0; width:100%;">
         <?php
         //including footer file
         include "../Footer.php";
         ?> 
   </div>
</body>
</html>
<?php
   if (isset($_POST['back'])) 
   {
      header("location:public_search_1.php");
   }
?>