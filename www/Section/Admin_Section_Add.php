<?php 
session_start();
if(!isset($_SESSION['login_user']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}
//Including database connection file
include "../connection.php" ; 
?>
<!DOCTYPE html>
<html>
<head>
<title>Admin_Section_Add</title>

<!--styles file including-->
<link rel="stylesheet" href="../ktfo_css.css">
</head>
<script>
	<!--confirmation message before add new section-->
	function submit_form()
	{
		return confirm("Do you really want to add");
	}
	<!--validation to enter Y / N for_application field-->
	function YN_validation()
	{
		var value= document.getElementById("for_application").value;
		if(value=="Y" || value=="N" )
		{ return;}
		else
		{
			document.getElementById("for_application").value="";
			window.alert("You should enter Y or N ");
		}
	}
	function YN_validation()
	{
		var value= document.getElementById("for_pass").value;
		if(value=="Y" || value=="N" )
		{ return;}
		else
		{
			document.getElementById("for_pass").value="";
			window.alert("You should enter Y or N ");
		}
	}
</script>
<body>
<!--Header-->
<div class="header" align="center">
  <img class="site_logo" height="100" id="logo" src="../gvt.jpg" alt="Kerala logo" >
  <h1>KOTTAYAM TALUK FRONT OFFICE</h1>
</div>
<!--navigation bar -->
<div class="navbar">
<a href="../Admin_Home.php">Home</a>
</div>
<br><br>
<!--form to add new section-->
<h3 align="center">Add New Section</h3>
<div class="form" >
<form id="section_add" name="section_add" method="post" action="" onSubmit="return submit_form()">
  <table>
    <tr>
      <td>Section Name<span> * </span></td>
      <td><input type="text" name="section_name" required="required"/></td>
    </tr>
	<tr>
	  <td>For Application<span> * </span>(Y/N)</td>
      <td><input type="text" id="for_application" name="for_application" required="required" onBlur="YN_validation()"/></td>
    </tr>
	<tr>
	  <td>For Pass<span> * </span>(Y/N)</td>
      <td><input type="text" id="for_pass" name="for_pass" required="required" onBlur="YN_validation()"/></td>
    </tr>
	<tr>
      <td>Section Purpose</td>
      <td><textarea name="purpose" cols="5" rows="5"></textarea></td>
    </tr>
    <tr>
      <td colspan="2" align="center"><button type="cancel" onClick="window.location='Admin_Section_View.php';return false;">CANCEL</button>
	  	 <button type="submit" name="add">ADD</button></td>
    </tr>
  </table>
</form>
</div>
<div style="bottom:0; width:100%;">
<?php
//including footer file
include "../Footer.php";
?> 
</body>
</html>
<?php
 if(isset($_POST['add']))
 {
 	$section_name=$_POST['section_name'];
	$for_application=$_POST['for_application'];
	$for_pass=$_POST['for_pass'];
	$purpose=$_POST['purpose'];
	
	//insert new section to table ktfo_section
	$sql="insert into ktfo_section(section_name,for_application,for_pass,section_purpose)values('$section_name','$for_application','$for_pass','$purpose')";
	
	if($conn->query($sql)== TRUE) 
	 	{ 
	?>
	<script>alert(" Added Successfully");</script> 
	<?php	
		header('location:Admin_Section_View.php'); //redirect to page Admin_Section_View.php 
		} 
	else
		{
	?>
  	<script> alert("failed");</script>  
<?php
		}
}
?>