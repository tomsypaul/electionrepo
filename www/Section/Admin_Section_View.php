<?php 
session_start();
if(!isset($_SESSION['login_user']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}

//Including database connection file
include "../connection.php" ; 
?>
<!DOCTYPE html>
<html>
<head>
  <title>Admin_Section_View</title>
  <link rel="stylesheet" href="../ktfo_css.css">
</head>
<body>
<!--Header-->
<div class="header" align="center">
  <img class="site_logo" height="100" id="logo" src="../gvt.jpg" alt="Kerala logo" >
  <h1>KOTTAYAM TALUK FRONT OFFICE</h1>
</div>
<!--navigation bar-->
<div class="navbar">
<a href="../Admin_Home.php">Home</a>
<a href="Admin_Section_Add.php">New Section</a>
</div>
<br><br>
<!--section view form-->
<div class="form">
		<form id="section_view" name="section_view" method="post" action="">
			<table  class="view_table">
  				<tr>
					<th>Section ID</th>
					<th>Section Name</th>
					<th>For Application</th>
					<th>For Pass</th>
					<th>Purpose</th>
					<th colspan="2"></th>
  				</tr>
<?php
	//fetching datas from table ktfo_section
	$records = mysqli_query($conn,"select * from ktfo_section"); 

	while($data = mysqli_fetch_array($records))
	{
?>
				<tr>
    				<td><?php echo $data['section_id']; ?></td>
					<td><?php echo $data['section_name']; ?></td>
					<td><?php echo $data['for_application']; ?></td> 
					<td><?php echo $data['for_pass']; ?></td>
					<td><?php echo $data['section_purpose']; ?></td>  
    				<td><a href="Admin_Section_Edit.php?section_id=<?php echo $data['section_id']; ?>">Edit</a></td>
    				<td><a href="Admin_Section_Delete.php?section_id=<?php echo $data['section_id']; ?>" onClick="return confirm('Are you sure to delete ?');">Delete</a>
				</tr>	
<?php
}
?>
			</table>
		</form>
	</div>
	<?php
		//including footer file
		include "../Footer.php";
	?>
</body>
</html>