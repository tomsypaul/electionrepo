<?php
session_start();
if(!isset($_SESSION['login_user']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}
?>
<!DOCTYPE html>
<html>
<head></head>
<body>
<?php
//fetch.php
//$connect = mysqli_connect("localhost", "root", "", "ktym_taluk");
//Including database connection file
include "../connection.php" ; 
$output = '';
if(isset($_POST["query"]))
{
   $search = mysqli_real_escape_string($conn, $_POST["query"]);
   $query = "SELECT ap.application_number,ap.person_id,ap.application_subject,ap.status,ap.section_current,pe.name,pe.mobile_number from ktfo_application ap JOIN ktfo_person pe ON ap.person_id=pe.person_id 
   WHERE ap.application_number LIKE '%".$search."%'
   OR ap.person_id LIKE '%".$search."%'  
   OR ap.status LIKE '%".$search."%' 
   OR ap.section_current LIKE '%".$search."%' 
   OR pe.name LIKE '%".$search."%'  
   OR pe.mobile_number LIKE '%".$search."%'";
}
else
{
   $query = "SELECT ap.application_number,ap.person_id,ap.application_subject,ap.status,ap.section_current,pe.name,pe.mobile_number FROM ktfo_application ap JOIN ktfo_person pe WHERE ap.person_id=pe.person_id";
}
$result = mysqli_query($conn, $query);
if(mysqli_num_rows($result) > 0)
{
?>
   <div class="table-responsive">
      <table class="table table bordered">
         <tr>
            <th>ApplicationNo</th>
            <th>Name</th>
            <th>Mobile</th>
            <th>Action</th>
         </tr>
<?php
   while($row = mysqli_fetch_array($result))
   {
 	   $cur_sec=$row['section_current'];
 ?>
         <tr>
            <td><?php echo $row['application_number']; ?> </td>
            <td><?php echo $row['name']; ?> </td>
            <td><?php echo $row['mobile_number']; ?> </td>
            <td>
               <form method="POST" action="Admin_Application_Edit.php" class="btnedit" >
                  <input type="hidden" name="application_number" value="<?php echo $row['application_number'];?>">
                  <input type="submit" name="edit" value="edit">
               </form>
            </td>           
         </tr>
<?php
   }
}
else
{
   echo 'Data Not Found';
}
?>
