<?php
    session_start();
    if(!isset($_SESSION['login_user']))
    {
	    echo "<script>alert('Session Expired');</script>";
	    echo '<script type="text/javascript">location.replace("../index.php");</script>';
    }
   include "../connection.php";
    
?>
<!DOCTYPE html>
<html>
<head>
	<title>Update File Number</title>
	<link rel="stylesheet" type="text/css" href="../ktfo_css.css">
	<style type="text/css">
		tr td:nth-child(1), tr td:nth-child(3) 
		{
            background: #f1f1f1;
        }
        input[type=text],input[type=date]
        {
        	border-radius: 10px;
        }
        input[type=date]
        {
        	font-size: 20px;
        }
        th,td
        {
        	padding: 17px 0px;
        }
        .linkbtn
        {
        	background-color: #f1f1f1;
        	text-decoration: none;
        	color: black;
        	padding: 15px 25px;
        	text-align: center;
        }
        .linkbtn:hover
        {
        	background-color: green;
        	color: white;
        }
        .search
        {
        	padding: 17px 25px;
        	font-size: 15px;
        	display: inline-block;
        }
	</style>
</head>
<body>
	<div class="header">
        <img class="site_logo" height="100" id="logo" src="../gvt.jpg" alt="Kerala logo" >
        <h1>Kottayam Taluk Front Office</h1>
    </div>
	<div class="navbar">
		<a href="../Admin_Home.php">Home</a>
	</div>
	<h1 class="header">Update File Number</h1>
	<div style="width: 100%; text-align: center;">
		<form method="POST">
			<input type="date" name="date" id="date" style="width: 50%" value="<?php echo date('Y-m-d',time()) ?>">
			<button type="submit" name="search" id="search" class="search">Search</button>
		</form>
	</div>
	<br><br>
	<form method="POST">
		<div id="list" style="width: 100%">
			<table>
				<tr style="text-align: left">
					<th>Application Number</th>
					<th>Applicant Name</th>
					<th>Application Purpose</th>
					<th>File Number</th>
					<th></th>
				</tr>
				<?php
				   
				    $date=date("Y-m-d");
				    $result=$conn->query("SELECT * FROM ktfo_application WHERE date_applied LIKE '$date%';");
				    if($result->num_rows>0)
				    {
				    	while($row=$result->fetch_assoc()) 
				    	{
				    		
				    		$application_no=$row['application_number'];
				    		$person_id=$row['person_id'];
				    		$purpose=$row['application_subject'];
				    		$file_no=$row['file_number'];
				    		$result1=$conn->query("SELECT name FROM ktfo_person WHERE person_id='$person_id';");
				    		if($result1->num_rows>0)
				            {
				    	        if($row1=$result1->fetch_assoc()) 
                                {
                                	$name=$row1['name'];
                                }
				            }
				            ?>
				            <tr>
				                <td><?php echo $application_no; ?></td>
				                <td><?php echo $name; ?></td>
				                <td><?php echo $purpose; ?></td>
				                <td><?php echo $file_no; ?></td>
				                <td><a href="Assign_FileNumber.php?apl_no=<?php echo $application_no; ?>" class="linkbtn">Update File number</a></td>
				            </tr>
				            <?php
				        }
				    }
				?>
			</table>
		</div>
	</form>
	<br><br><br>
	<div style="position:fixed; bottom:0; width:100%;">
         <?php
         //including footer file
         include "../Footer.php";
         ?> 
   </div>
</body>
</html>
<?php
    
    if (isset($_REQUEST['search'])) 
    {
    	$date=date('Y-m-d',strtotime($_REQUEST['date']));
    	$result=$conn->query("SELECT * FROM ktfo_application WHERE date_applied LIKE '$date%';");
    	?>
    	<script type="text/javascript">
            var content='<table><tr style="text-align: left"><th>Application Number</th><th>Applicant Name</th><th>Application Purpose</th><th>File Number</th><th></th></tr>';
    	</script>
    	<?php
    	if ($result->num_rows>0) 
    	{
    		while($row=$result->fetch_assoc()) 
			{
				
				$application_no=$row['application_number'];
				$person_id=$row['person_id'];
				$purpose=$row['application_subject'];
				$file_no=$row['file_number'];
				$result1=$conn->query("SELECT name FROM ktfo_person WHERE person_id='$person_id';");
				if($result1->num_rows>0)
				{
				    if($row1=$result1->fetch_assoc()) 
                    {
                        $name=$row1['name'];
                    }
				}
			?>
			    <script type="text/javascript">
				    content+='<tr><td><?php echo $application_no; ?></td><td><?php echo $name; ?></td><td><?php echo $purpose; ?></td><td><?php echo $file_no; ?></td><td><a href="Assign_FileNumber.php?apl_no=<?php echo $application_no; ?>" class="linkbtn">Update File Number</a</td></tr>';
			    </script>
			<?php
			}	            
    	}
    	?>
    	<script type="text/javascript">
    		content+="</table>";
			document.getElementById('list').innerHTML=content;
    		document.getElementById('date').value="<?php echo $date ?>";
    	</script>
    <?php	
    }
?>


