<?php 
session_start();

$application_number= $_SESSION['application_number'];// application number for retriving personal details

if(!isset($_SESSION['login_user']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}

//Including database connection file
include "../connection.php" ; 

$query = " select * from ktfo_application where application_number='".$application_number."'";
$result = mysqli_query($conn,$query);
while($row=mysqli_fetch_assoc($result))
	{
  		$person_id = $row['person_id'];
		$stats=$row['status'];
		
	
?>
<!DOCTYPE html>
<html>
<head>
  <title>Application_Update_Status</title>
  <link rel="stylesheet" href="../ktfo_css.css">
</head>
<body>
<!--Header-->
<div class="header" align="center">
  <img class="site_logo" height="100" id="logo" src="../gvt.jpg" alt="Kerala logo" >
  <h1>KOTTAYAM TALUK FRONT OFFICE</h1>
</div>
<!--navigation bar-->
<div class="navbar">
<a href="../Admin_Home.php">Home</a>
</div>
<br><br>
<body>
<div class="form">
<form action="" method="post">
<table>
  <tr>
    <td>Application Number</td>
    <td><input name="application_number" type="text" disabled="disabled" value="<?php echo $application_number;?>"></td>
  </tr>
  <tr>
    <td>Name</td>
<?php 
	$qry = "select * from ktfo_person where person_id='$person_id'";
    $res = mysqli_query($conn,$qry);
 
 	while($data=mysqli_fetch_assoc($res))
	 {
  		$name = $data['name'];	
	
?>
    <td><input name="name" type="text" disabled="disabled" value="<?php echo $name; ?>"></td>
  </tr>

  <tr>
    <td><label for="status">Status</label></td>
	<td>
<input type="text" list="status" name="status" id="stat" required value="<?php echo $stats; }}?>">
<datalist id="status">
  <option value="Additional document required">
  <option value="DCs permission needed">
  <option value="Disposed">
  <option value="Ordered">
  <option value="Processing">
   <option value="Site inspection required">
    <option value="Surveyors report awaited">
	 <option value="V.O report awaited">
	  <option value="With Head Surveyor">
	   <option value="With JS">
	    <option value="With Other Office">
		 <option value="With Tahsildar">
		  <option value="With village office">
</datalist></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><button name="update" type="submit" class="btn">Update</button></td>
  </tr>
</table>
</div>
</form>
<br><br>
<div style=" bottom:0; width:100%;">
<?php
//including footer file
include "../Footer.php";
?> 
</div>
</body>
</html>
<?php
	if(isset($_POST['update']))
	{
		$status=$_POST['status'];
		
		$sql="update ktfo_application set status='$status' where application_number='".$application_number."'";
		if($conn->query($sql)== TRUE)
	 	{ 
	?>
	<script>confirm(" Updated Successfully");</script> 
	<?php	
		header('location:Application_Update_Status.php');  
		} 
	else
		{
	?>
  	<script> alert("failed");</script>  
<?php
		}
	}
?>