<?php
session_start();
if(!isset($_SESSION['login_user']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}
?>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>search</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />
  <!--styles file including-->
<link rel="stylesheet" href="../ktfo_css.css">
 </head>
 <body>
 <!--Header-->
<div class="header" align="center">
  <img class="site_logo" height="100" id="logo" src="../gvt.jpg" alt="Kerala logo" >
  <h1>KOTTAYAM TALUK FRONT OFFICE</h1>
</div>
<!--navigation bar -->
<div class="navbar">
<a href="../Admin_Home.php">Home</a>
</div>
<br><br>
  <div class="container">
   <br />
   <h2 align="center"> Search Application </h2><br />
   <div class="form-group">
    <div class="input-group">
     <span class="input-group-addon">Search</span>
     <input type="text" name="search_text" id="search_text" placeholder="Search here" class="form-control" /><br>
    </div>
   </div>
   <br />
   <div id="result"></div>
  </div>
  <?php
		//including footer file
		include "../Footer.php";
	?>
 </body>
</html>


<script>
$(document).ready(function(){

 load_data();

 function load_data(query)
 {
  $.ajax({
   url:"fetch.php",
   method:"POST",
   data:{query:query},
   success:function(data)
   {
    $('#result').html(data);
   }
  });
 }
 $('#search_text').keyup(function(){
  var search = $(this).val();
  if(search != '')
  {
   load_data(search);
  }
  else
  {
   load_data();
  }
 });
});
</script>



