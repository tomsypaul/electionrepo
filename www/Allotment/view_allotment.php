<?php
session_start();
if(!isset($_SESSION['login_user']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}

//including connection file
    include "../connection.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<!--including css file-->
	<link rel="stylesheet" type="text/css" href="../css.css">
</head>
<body>

	<!--including menu file-->
	<?php include "../menu.php"; ?>


	<h2 class="header" style = "Arial,Garamond,Sans-serif;font-size:20px;">ORDER OF THE APPOINTMENT OF POLLING OFFICERS FOR ABSENTEE VOTERS</h3>

<div class="form">
		<form id="section_view" name="section_view" method="post" action="printing_individual.php">
			<table  class="view_table">
  				<tr>
					<th>Team Id</th>
					<th>Micro Observer</th>
					<th>Official 1</th>
					<th>Official 2</th>
					<th>Village</th>
					<!--<th colspan="2"></th>-->
  				</tr>
<?php
	//fetching datas from table ktfo_section
	$records = mysqli_query($conn,"SELECT team_id, e1.name as name1, e2.name as name2, village_name,e3.name as name3 FROM election_allotment_details
		 INNER JOIN election_official1 e1 ON e1.official1_id = election_allotment_details.official1_id
		  INNER JOIN election_official2 e2 ON e2.official2_id = election_allotment_details.official2_id
		 INNER JOIN election_village ON election_village.village_code =election_allotment_details.village_code
		 INNER JOIN election_observer e3 ON e3.observer_id = election_allotment_details.observer_id");
	while($data = mysqli_fetch_assoc($records))
	{
?>
				<tr>
    				<td><?php echo $data['team_id']; ?></td>
						<td><?php echo $data['name3']; ?></td>
					<td><?php echo $data['name1']; ?></td>
					<td><?php echo $data['name2']; ?></td>
					<td><?php echo $data['village_name']; ?></td>
				</tr>

<?php
}
?>
	    </table>
	</form>
	</div>
<div style="position:relative; bottom:0; width:100%;">
<?php
//including footer file
include "../Footer.php";
?>
</body>
</html>
