<html>
<head>
  <title>Home Page</title>
  <link rel="stylesheet" href="ktfo_home.css">
</head>
<body><font type="Times">
	<!--Header-->
	<div class="header" align="center">
		<img class="site_logo" height="100" id="logo" src="gvt.jpg" alt="Kerala logo" >
		<h1 style="font-family: "Times New Roman", Times, serif;"><b>LAC ELECTION 2021</b></h1>
		<h2 style="font-family: "Times New Roman", Times, serif;"><b>KOTTAYAM DISTRICT</b></h2>
		<div class="navbar">
		  <a href="Admin_Home.php">Home</a>

			<div class="dropdown">
				<button class="dropbtn">Upload Data
				  <i class="fa fa-caret-down"></i>
				</button>
				<div class="dropdown-content">
				  <a href="excel/sample1.php">Official 1</a>
				  <a href="excel/sample2.php">Official 2</a>
				   <a href="excel/sample3.php">Micro observer</a>
				   <a href="excel/requirement.php">Election requirement</a>
				</div>
			</div>
			<a href="Allotment/allottment_process.php">Allot Officials</a>
			<div class="dropdown">
				<button class="dropbtn">Allotment Print
				  <i class="fa fa-caret-down"></i>
				</button>
				<div class="dropdown-content">
				  <a href="Print/combined_ui.php">Institutional Order & Appointment Order</a>
				  <a href="Print/Individual_print.php">Appointment Order</a>
				  <a href="Print/Institution_Print.php">Institutional Order</a>
				</div>
			</div>
			<a href="Allotment/view_allotment.php">View Allotment</a>
			<div class="dropdown">
				<button class="dropbtn">Edit
				  <i class="fa fa-caret-down"></i>
				</button>
				<div class="dropdown-content">
				  <a href="edit/official1_edit.php">Official 1</a>
				  <a href="edit/official2_edit.php">Official 2</a>
				  <a href="edit/observer_edit.php">Micro observer</a>
				</div>
			</div>
			<a href="Logout.php" style="float:right">Logout</a>
			<a href="change_pswrd.php" style="float:right">Change Password</a>
		</div>
	</div>
	<br><br>
</body>
</html>

