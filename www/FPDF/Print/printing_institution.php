<?php
include '../connection.php';
require_once('../FPDF/fpdf.php');
require_once('../FPDI/src/autoload.php');
use \setasign\Fpdi\Fpdi;


$office=mysqli_query($conn,"select DISTINCT office_name from ((select  * from election_official1 where status='Allotted')UNION(select  * from election_official2 where status='Allotted'))as p");


generatePDF("institution.pdf", "output.pdf",$office,$conn);

function generatePDF($source, $output,$office,$conn)
{
      $pdf = new FPDI();
         // 'Landscape','mm',array(210,148)
         /*$pdf->AddPage(); //Add A4 by Default
         $pdf->SetFont('TImes','',12);
         $pagecount = $pdf->setSourceFile($source);
         $tppl = $pdf->importPage(1);
         $pdf->useTemplate($tppl, 0, 0);

         $width_cell=array(10,30,20,30);


         $pdf->SetXY(24.4,180.3);
         $pdf->Cell(10,9,'Sl. No',1,0,true);
         $pdf->Cell(55,9,'Name',1,0,true);
         $pdf->Cell(55,9,'Designation',1,0,true);
         $pdf->Cell(40,9,'Category',1,0,true);
         $pdf->Cell(25,9,'Signature',1,0,true);*/

        while($data = mysqli_fetch_assoc($office))
        {

          $count=1;
          $pdf->AddPage();
                  $pdf->SetFont('TImes','B',12);
                   $pdf->SetXY(22.0,180.3);
                   $pdf->Cell(12,9,'Sl. No',1,0,'L');
                   $pdf->Cell(49,9,'Name',1,0,'L');
                   $pdf->Cell(49,9,'Designation',1,0,'L');
                   $pdf->Cell(40,9,'Category',1,0,'L');
                   $pdf->Cell(20,9,'Signature',1,0,'L');

          $details=mysqli_query($conn,"(SELECT * from election_official1 as p1 WHERE p1.status='Allotted') UNION (SELECT * from election_official2 as p2 WHERE p2.status='Allotted')");
          $x=190.3;
        while ($row = mysqli_fetch_assoc($details))
        {

          if($row['office_name']==$data['office_name'])
          {

                    $office_name =$row['office_name'];
                    $designation=$row['designation'];
        						$name=$row['name'];
                    $election_id=$row['election_id'];


         //Add A4 by Default
        $pagecount = $pdf->setSourceFile($source);
        $tppl = $pdf->importPage(1);

        $pdf->useTemplate($tppl, 0, 0); //need to be set , 210, 148



        $pdf->SetFont('Times','',11); // Font Name, Font Style (eg. 'B' for Bold), Font Size
        $pdf->SetTextColor(0,0,0); // RGB
        $pdf->SetXY(37.4,76.1); // X start, Y start in mm
        $pdf->Write(0, $office_name);

        $sql1=mysqli_query($conn,"select * from election_official1 where election_id='$election_id'");
        $sql2=mysqli_query($conn,"select * from election_official2 where election_id='$election_id'");
      if( mysqli_num_rows($sql1)>0)
          $category='Official 1';
          if( mysqli_num_rows($sql2)>0)
              $category='Official 2';

        $pdf->SetXY(22.0,$x);
        $pdf->Cell(12,9,$count,1,0,'L');
        $pdf->Cell(49,9,$name,1,0,'L');
        $pdf->Cell(49,9,$designation,1,0,'L');
        $pdf->Cell(40,9,$category,1,0,'L');
        $pdf->Cell(20,9,'',1,0,'L');

        $count++;
        $x=$x+8;
        }
        }
      }
        $pdf->Output($output, "I");

}
?>
