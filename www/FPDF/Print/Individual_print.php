<?php
session_start();
if(!isset($_SESSION['login_user']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}

//including connection file
    include "../connection.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>

	<!--including css file-->
	<link rel="stylesheet" type="text/css" href="../ktfo_css.css">
</head>
<body>
	<div class="header">
        <img class="site_logo" height="100" id="logo" src="../gvt.jpg" alt="Kerala logo">
        <h1>LAC Kottayam 2021</h1>
    </div>
	<div class="navbar">
		<a href="../Admin_Home.php">Home</a>

	</div>
	<h1 class="header">ORDER OF THE APPOINTMENT OF POLLING OFFICERS FOR ABSENTEE VOTERS</h1>

<div class="form">
		<form id="section_view" name="section_view" method="post" action="printing_individual.php">
			<table  class="view_table">
  				<tr>
					<th>Team Id</th>
					<th>Official 1</th>
					<th>Official 2</th>
					<th>Village</th>
					<!--<th colspan="2"></th>-->
  				</tr>
<?php
	//fetching datas from table ktfo_section
	$records = mysqli_query($conn,"SELECT team_id, e1.name as name1, e2.name as name2, village_name FROM election_allotment_details INNER JOIN election_official1 e1 ON e1.official1_id = election_allotment_details.official1_id INNER JOIN election_official2 e2 ON e2.official2_id = election_allotment_details.official2_id INNER JOIN election_village ON election_village.village_code =election_allotment_details.village_code");

	while($data = mysqli_fetch_assoc($records))
	{
?>
				<tr>
    				<td><?php echo $data['team_id']; ?></td>
					<td><?php echo $data['name1']; ?></td>
					<td><?php echo $data['name2']; ?></td>
					<td><?php echo $data['village_name']; ?></td>
				</tr>

<?php
}
?>
    <!--<tr>
      <td colspan="4"> <button type="submit" name="submit" class="submit" >Print</button> </td>
    </tr>-->
			</table>
      <p align="center">
      <button type="submit" name="submit" class="submit" >Print</button></p>
		</form>
	</div>

	<?php
		//including footer file
		include "../Footer.php";
	?>
</body>
</html>
