<?php
session_start();
if(!isset($_SESSION['login_user']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}

//including connection file
    include "../connection.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>

	<!--including css file-->
	<link rel="stylesheet" type="text/css" href="../ktfo_css.css">
</head>
<body>
	<div class="header">
        <img class="site_logo" height="100" id="logo" src="../gvt.jpg" alt="Kerala logo">
        <h1>LAC Kottayam 2021</h1>
    </div>
	<div class="navbar">
		<a href="../Admin_Home.php">Home</a>

	</div>
	<h1 class="header">General Election to KLA 2021 – Appointment of polling officers
 of absentee voters – posting order serving – reg</h1>

<div class="form">
		<form id="section_view" name="section_view" method="post" action="printing_institution.php">

<?php
	$office=mysqli_query($conn,"select DISTINCT office_name from ((select  * from election_official1 where status='Allotted')UNION(select  * from election_official2 where status='Allotted'))as p");

    while($data = mysqli_fetch_assoc($office))
	{
    $count=1;
    ?>
    <table  class="view_table">
              <tr><td colspan="5" align="center"><b><?php echo $data['office_name'];  ?></b></td><tr>
        <tr>
                  <th>Sl No</th>
        <th>Name</th>
        <th>Designation</th>
        <th>Category</th>
                  <th>Signature</th>
        <!--<th colspan="2"></th>-->
        </tr>
        <?php
        $details=mysqli_query($conn,"((SELECT * from election_official1 as p1 WHERE p1.status='Allotted') UNION (SELECT * from election_official2 as p2 WHERE p2.status='Allotted'))");

      while ($row = mysqli_fetch_assoc($details))
      {

        if($row['office_name']==$data['office_name'])
        {
          ?>
          <tr>
              <td><?php echo $count; ?></td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['designation']; ?></td>
            <td><?php echo $row['clazz']; ?></td>
          </tr>
        <?php
        $count++;
        }

      }
      ?>
    </table> <br><br><br>
      <?php
}
 ?>


      <p align="center">
      <button type="submit" name="submit" class="submit" >Print</button></p>
		</form>
	</div>

	<?php
		//including footer file
		include "../Footer.php";
	?>
</body>
</html>
