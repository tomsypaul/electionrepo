<?php
include '../connection.php';
require_once('../FPDF/fpdf.php');
require_once('../FPDI/src/autoload.php');
use \setasign\Fpdi\Fpdi;


$records = mysqli_query($conn,"SELECT e1.name as name1,e1.designation as designation1,e1.office_code_no as office_code_no_1 ,e1.office_name as office_name_1,e1.mobile_phone_no as mob1,
e2.name as name2,e2.designation as designation2,e2.office_code_no as office_code_no_2,e2.office_name as office_name_2,e1.mobile_phone_no as mob2 FROM election_allotment_details
 INNER JOIN election_official1 e1 ON e1.official1_id = election_allotment_details.official1_id
 INNER JOIN election_official2 e2 ON e2.official2_id = election_allotment_details.official2_id");
generatePDF("appointment.pdf", "output.pdf",$records);


 function generatePDF($source, $output,$records)
 {

$pdf = new FPDI(); // 'Landscape','mm',array(210,148)
while($data = mysqli_fetch_assoc($records))
{


            $official1_name=$data['name1'];
            $official1_designation=$data['designation1'];
            $official1_phone_number=$data['mob1'];
            $official1_office_id=$data['office_code_no_1'];
            $official1_office_name=$data['office_name_1'];
						$official2_name=$data['name2'];
            $official2_designation=$data['designation2'];
            $official2_phone_number=$data['mob2'];
            $official2_office_id=$data['office_code_no_2'];
            $official2_office_name=$data['office_name_2'];

$pdf->AddPage(); //Add A4 by Default
$pagecount = $pdf->setSourceFile($source);
$tppl = $pdf->importPage(1);

$pdf->useTemplate($tppl, 0, 0); //need to be set , 210, 148



$pdf->SetFont('Arial','',12); // Font Name, Font Style (eg. 'B' for Bold), Font Size
$pdf->SetTextColor(0,0,0); // RGB
$pdf->SetXY(30,80); // X start, Y start in mm
$pdf->Write(0, $official1_name);

$pdf->SetFont('Arial','',12); // Font Name, Font Style (eg. 'B' for Bold), Font Size
$pdf->SetTextColor(0,0,0); // RGB
$pdf->SetXY(30,86); // X start, Y start in mm
$pdf->Write(0, $official1_designation);

$pdf->SetFont('Arial','',12); // Font Name, Font Style (eg. 'B' for Bold), Font Size
$pdf->SetTextColor(0,0,0); // RGB
$pdf->SetXY(30,92); // X start, Y start in mm
$pdf->Write(0, $official1_office_name);


$pdf->SetFont('Arial','',12); // Font Name, Font Style (eg. 'B' for Bold), Font Size
$pdf->SetTextColor(0,0,0); // RGB
$pdf->SetXY(30,98); // X start, Y start in mm
$pdf->Write(0, $official1_office_id);


$pdf->SetFont('Arial','',12); // Font Name, Font Style (eg. 'B' for Bold), Font Size
$pdf->SetTextColor(0,0,0); // RGB
$pdf->SetXY(30,104); // X start, Y start in mm
$pdf->Write(0, $official1_phone_number);

$pdf->SetFont('Arial','',12); // Font Name, Font Style (eg. 'B' for Bold), Font Size
$pdf->SetTextColor(0,0,0); // RGB
$pdf->SetXY(110,80); // X start, Y start in mm
$pdf->Write(0, $official2_name);

$pdf->SetFont('Arial','',12); // Font Name, Font Style (eg. 'B' for Bold), Font Size
$pdf->SetTextColor(0,0,0); // RGB
$pdf->SetXY(110,86); // X start, Y start in mm
$pdf->Write(0, $official2_designation);

$pdf->SetFont('Arial','',12); // Font Name, Font Style (eg. 'B' for Bold), Font Size
$pdf->SetTextColor(0,0,0); // RGB
$pdf->SetXY(110,92); // X start, Y start in mm
$pdf->Write(0, $official2_office_name);


$pdf->SetFont('Arial','',12); // Font Name, Font Style (eg. 'B' for Bold), Font Size
$pdf->SetTextColor(0,0,0); // RGB
$pdf->SetXY(110,98); // X start, Y start in mm
$pdf->Write(0, $official2_office_id);


$pdf->SetFont('Arial','',12); // Font Name, Font Style (eg. 'B' for Bold), Font Size
$pdf->SetTextColor(0,0,0); // RGB
$pdf->SetXY(110,104); // X start, Y start in mm
$pdf->Write(0, $official2_phone_number);

}
$pdf->Output($output, "I");
}
?>
