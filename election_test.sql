-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 15, 2021 at 09:41 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

/*
SELECT  election_village.lac_number as Req_LAC,e4.l1 as LAC1,e4.l2 as LAC2,e4.l3 as LAC3, election_allotment_details.village_code as Req_Village_Code, team_id, e1.name as name1,e1.lac_no as Off1_Lac,e1.reg_lac_no as Reg_Lac_Off1,e1.reside_lac_no as Reside_Lac_Off1, e2.name as name2,e2.lac_no as Off2_Lac,e2.reg_lac_no as Reg_Lac_Off2,e2.reside_lac_no as Reside_Lac_Off2,e3.name as name3,e3.lac_no as Observer_Lac,e3.reg_lac_no as Reg_Lac_Obsrvr,e3.reside_lac_no as Reside_Lac_Obsrvr 
FROM election_allotment_details
		 INNER JOIN election_official1 e1 ON e1.official1_id = election_allotment_details.official1_id
		  INNER JOIN election_official2 e2 ON e2.official2_id = election_allotment_details.official2_id
		 INNER JOIN election_village ON election_village.village_code =election_allotment_details.village_code
		 INNER JOIN election_observer e3 ON e3.observer_id = election_allotment_details.observer_id
INNER JOIN election_nearbylac e4 ON e4.lac_number = election_village.lac_number;
*/


SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
CREATE USER IF NOT EXISTS 'developer'@'localhost' IDENTIFIED BY 'passwd#1234';


DROP DATABASE IF EXISTS election_test;
CREATE DATABASE election_test;

GRANT ALL PRIVILEGES ON election_test . * TO 'developer'@'localhost';



USE election_test;

--
-- Database: `election_test`
--

-- --------------------------------------------------------
-- --------------------------------------------------------

--
-- Table structure for table `election_login`
--

CREATE TABLE `election_login` (
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `election_login`
--

INSERT INTO `election_login` (`username`, `password`) VALUES
('admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `election_official1`
--

CREATE TABLE `election_official1` (
  `official1_id` int(11) NOT NULL,
  `name` char(50) DEFAULT NULL,
  `village_code` int(11) DEFAULT NULL,
  `dist_no` int(11) DEFAULT NULL,
  `office_type_id` int(11) DEFAULT NULL,
  `office_name` varchar(100) DEFAULT NULL,
  `address1` varchar(50) DEFAULT NULL,
  `address2` varchar(50) NOT NULL,
  `address3` varchar(50) NOT NULL,
  `phone_no` bigint(10) NOT NULL,
  `lac_no` int(11) NOT NULL,
  `office_code_no` bigint(20) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `pen` int(11) NOT NULL,
  `gazetted` int(11) NOT NULL,
  `clazz` int(11) NOT NULL,
  `residence_phone_no` bigint(20) NOT NULL,
  `mobile_phone_no` bigint(20) NOT NULL,
  `reg_lac_no` int(11) NOT NULL,
  `reside_lac_no` int(11) NOT NULL,
  `election_id` varchar(20) NOT NULL,
  `sex` char(1) NOT NULL,
  `retirement_date` int(11) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `election_official2`
--

CREATE TABLE `election_official2` (
  `official2_id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `village_code` int(11) DEFAULT NULL,
  `dist_no` int(11) DEFAULT NULL,
  `office_type_id` int(11) NOT NULL,
  `office_name` varchar(100) NOT NULL,
  `address1` varchar(50) NOT NULL,
  `address2` varchar(50) NOT NULL,
  `address3` varchar(50) NOT NULL,
  `phone_no` bigint(20) NOT NULL,
  `lac_no` int(11) NOT NULL,
  `office_code_no` bigint(20) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `pen` int(11) NOT NULL,
  `gazetted` int(11) NOT NULL,
  `clazz` int(11) NOT NULL,
  `residence_phone_no` bigint(20) NOT NULL,
  `mobile_phone_no` bigint(20) NOT NULL,
  `reg_lac_no` int(11) NOT NULL,
  `reside_lac_no` int(11) NOT NULL,
  `election_id` varchar(20) NOT NULL,
  `sex` char(1) NOT NULL,
  `retirement_date` int(11) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `election_requirement`
--

CREATE TABLE `election_requirement` (
  `village_code` int(11) NOT NULL,
  `required` int(11) NOT NULL,
  `allotted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `election_village`
--

CREATE TABLE `election_village` (
  `village_code` int(11) NOT NULL,
  `Dist_no` int(11) NOT NULL,
  `village_name` varchar(20) NOT NULL,
  `lac_number` int(3) NOT NULL,
  `taluk_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `election_village`
--

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'RAMAPURAM',93,'MEENACHIL',10001);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'VELLILAPPALLY',93,'MEENACHIL',10002);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'KADANADU',93,'MEENACHIL',10003);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'MELUKAVU',93,'MEENACHIL',10004);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'MOONNILAVU',93,'MEENACHIL',10005);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'THALANDAU',93,'MEENACHIL',10006);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'THALAPPALAM',93,'MEENACHIL',10007);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'BHARANANGANAM',93,'MEENACHIL',10008);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'LALAM',93,'MEENACHIL',10009);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'VALLICHIRA',93,'MEENACHIL',10010);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'PULIYANNOOR',93,'MEENACHIL',10011);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'MEENACHIL',93,'MEENACHIL',10012);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'POOVARANY',93,'MEENACHIL',10013);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'ELIKKULAM',93,'MEENACHIL',10014);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'ELANGULAM',93,'MEENACHIL',10015);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'MULAKKULAM',94,'MEENACHIL',10016);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'KADUTHURUTHY',94,'MEENACHIL',10017);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'VELIYANNOOR',94,'MEENACHIL',10018);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'MONIPPALLY',94,'MEENACHIL',10019);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'UZHAVOOR',94,'MEENACHIL',10020);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'NJEEZHOOR',94,'MEENACHIL',10021);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'MUTTUCHIRA',94,'MEENACHIL',10022);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'KURAVILANGADU',94,'MEENACHIL',10023);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'KURICHITHANAM',94,'MEENACHIL',10024);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'ELACKADU',94,'MEENACHIL',10025);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'MANJOOR',94,'MEENACHIL',10026);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'KOTHANALLOOR',94,'MEENACHIL',10027);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'KIDANGOOR',94,'MEENACHIL',10028);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'KANAKKARY',94,'MEENACHIL',10029);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'CHEMPU',95,'VAIKOM',10030);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'VELLOOR',95,'VAIKOM',10031);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'KULASEKHARAMANGALAM',95,'VAIKOM',10032);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'VADAKKEMURY',95,'VAIKOM',10033);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'NADUVILE',95,'VAIKOM',10034);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'VAIKOM',95,'VAIKOM',10035);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'VADAYAR',95,'VAIKOM',10036);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'THALAYAZHAM',95,'VAIKOM',10037);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'KALLARA',95,'VAIKOM',10038);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'VECHOOR',95,'VAIKOM',10039);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'ONAMTHURUTHU',96,'KOTTAYAM',10040);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'KAIPPUZHA',96,'KOTTAYAM',10041);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'ATHIRAMPUZHA',96,'KOTTAYAM',10042);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'ETTUMANOOR',96,'KOTTAYAM',10043);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'PEROOR',96,'KOTTAYAM',10044);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'ARPOOKARA',96,'KOTTAYAM',10045);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'AYMANAM',96,'KOTTAYAM',10046);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'KUMARAKOM',96,'KOTTAYAM',10047);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'CHENGALAM SOUTH',96,'KOTTAYAM',10048);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'THIRUVARPPU',96,'KOTTAYAM',10049);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'PERUMBAIKKADU',97,'KOTTAYAM',10050);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'VIJAYAPURAM',97,'KOTTAYAM',10051);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'MUTTAMBALAM',97,'KOTTAYAM',10052);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'KOTTAYAM',97,'KOTTAYAM',10053);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'VELOOR',97,'KOTTAYAM',10054);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'NATTAKOM',97,'KOTTAYAM',10055);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'PANACHIKKADU',97,'KOTTAYAM',10056);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'AYARKKUNNAM',98,'KOTTAYAM',10057);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'AKALAKKUNNAM',98,'KOTTAYAM',10058);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'CHENGALAM EAST',98,'KOTTAYAM',10059);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'KOOROPPADA',98,'KOTTAYAM',10060);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'MANARCADU',98,'KOTTAYAM',10061);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'PAMABADY',98,'KOTTAYAM',10062);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'PUTHUPPALLY',98,'KOTTAYAM',10063);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'MEENADOM',98,'KOTTAYAM',10064);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'VAKATHANAM',98,'KOTTAYAM',10065);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'THOTTACKADU',98,'KOTTAYAM',10066);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'KURICHI',99,'CHANGANASSERRY',10067);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'VAZHAPPALLY(W)',99,'CHANGANASSERRY',10068);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'CHETHIPUZHA',99,'CHANGANASSERRY',10069);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'MADAPPALLY',99,'CHANGANASSERRY',10070);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'THRICKODITHANAM',99,'CHANGANASSERRY',10071);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'VAZHAPPALLY(E)',99,'CHANGANASSERRY',10072);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'CHANGANACHERRY',99,'CHANGANASSERRY',10073);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'PAIPPADU',99,'CHANGANASSERRY',10074);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'ANICKAD',100,'KANJIRAPPALLY',10075);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'KANJIRAPPALLY',100,'KANJIRAPPALLY',10076);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'KOOVAPPALLY',100,'KANJIRAPPALLY',10077);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'CHIRAKKADAVU',100,'KANJIRAPPALLY',10078);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'CHERUVALLY',100,'KANJIRAPPALLY',10079);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'VAZHOOR',100,'KANJIRAPPALLY',10080);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'KARUKACHAL',100,'KANJIRAPPALLY',10081);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'NEDUMKUNNAM',100,'KANJIRAPPALLY',10082);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'KANGAZHA',100,'KANJIRAPPALLY',10083);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'VELLAVOOR',100,'KANJIRAPPALLY',10084);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'MANIMALA',100,'KANJIRAPPALLY',10085);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'ERATTUPETTA',101,'KANJIRAPPALLY',10086);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'TEEKOY',101,'KANJIRAPPALLY',10087);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'POONJAR THEKKEKARA',101,'KANJIRAPPALLY',10088);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'POONJAR NADUBHAGAM',101,'KANJIRAPPALLY',10089);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'KONDOOR',101,'KANJIRAPPALLY',10090);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'EDAKKUNNAM',101,'KANJIRAPPALLY',10091);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'MUNDAKKAYAM',101,'KANJIRAPPALLY',10092);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'KOOTTICKAL',101,'KANJIRAPPALLY',10093);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'ERUMELY NORTH',101,'KANJIRAPPALLY',10094);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'ERUMELY SOUTH',101,'KANJIRAPPALLY',10095);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'KORUTHODE',101,'KANJIRAPPALLY',10096);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'POONJAR',101,'MEENACHIL',10097);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'KADAPLAMATTOM',94,'MEENACHIL',10098);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'UDAYANAPURAM',95,'VAIKOM',10099);

INSERT INTO election_village(Dist_no,village_name,lac_number,taluk_name,village_code) VALUES (10,'T V PURAM',95,'VAIKOM',10100);



--
-- Indexes for dumped tables
--
--
-- Indexes for table `election_login`
--
ALTER TABLE `election_login`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `election_official1`
--
ALTER TABLE `election_official1`
  ADD PRIMARY KEY (`official1_id`),
  ADD KEY `foreign key Official 2` (`village_code`);

--
-- Indexes for table `election_official2`
--
ALTER TABLE `election_official2`
  ADD PRIMARY KEY (`official2_id`),
  ADD KEY `village_fk` (`village_code`);

--
-- Indexes for table `election_requirement`
--
ALTER TABLE `election_requirement`
  ADD KEY `village_code` (`village_code`);

--
-- Indexes for table `election_village`
--
ALTER TABLE `election_village`
  ADD PRIMARY KEY (`village_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `election_allotment_details`
--
--
-- AUTO_INCREMENT for table `election_official1`
--
ALTER TABLE `election_official1`
  MODIFY `official1_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `election_official2`
--
ALTER TABLE `election_official2`
  MODIFY `official2_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `election_allotment_details`
--

--
-- Constraints for table `election_official1`
--
ALTER TABLE `election_official1`
  ADD CONSTRAINT `foreign key Official 2` FOREIGN KEY (`village_code`) REFERENCES `election_village` (`village_code`);

--
-- Constraints for table `election_official2`
--
ALTER TABLE `election_official2`
  ADD CONSTRAINT `village_fk` FOREIGN KEY (`village_code`) REFERENCES `election_village` (`village_code`);

--
-- Constraints for table `election_requirement`
--
ALTER TABLE `election_requirement`
  ADD CONSTRAINT `election_requirement_ibfk_1` FOREIGN KEY (`village_code`) REFERENCES `election_village` (`village_code`);
COMMIT;

CREATE TABLE election_nearbylac(
  lac_number INTEGER  NOT NULL PRIMARY KEY 
 ,lac_name   VARCHAR(13) NOT NULL
 ,l1         INTEGER  NOT NULL
 ,l2         INTEGER  NOT NULL
 ,l3         INTEGER  NOT NULL
);
INSERT INTO election_nearbylac(lac_number,lac_name,l1,l2,l3) VALUES
 (93,'PALA',94,101,98)
,(94,'KADUTHURUTHY',95,96,93)
,(95,'VAIKOM',94,96,93)
,(96,'ETTUMANOOR',94,97,93)
,(97,'KOTTAYAM',98,96,99)
,(98,'PUTHUPPALLY',97,99,96)
,(99,'CHANGANASSERY',98,100,97)
,(100,'KANJIRAPPALLY',101,98,99)
,(101,'POONJAR',100,93,98);


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

CREATE TABLE `election_observer` (
  `observer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(50) DEFAULT NULL,
  `village_code` int(11) NOT NULL,
  `dist_no` int(11) DEFAULT NULL,
  `office_type_id` int(11) DEFAULT NULL,
  `office_name` varchar(100) DEFAULT NULL,
  `address1` varchar(50) DEFAULT NULL,
  `address2` varchar(50) NOT NULL,
  `address3` varchar(50) NOT NULL,
  `phone_no` bigint(10) NOT NULL,
  `lac_no` int(11) NOT NULL,
  `office_code_no` bigint(20) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `pen` int(11) NOT NULL,
  `gazetted` int(11) NOT NULL,
  `clazz` int(11) NOT NULL,
  `residence_phone_no` bigint(20) NOT NULL,
  `mobile_phone_no` bigint(20) NOT NULL,
  `reg_lac_no` int(11) NOT NULL,
  `reside_lac_no` int(11) NOT NULL,
  `election_id` varchar(20) NOT NULL,
  `sex` char(1) NOT NULL,
  `retirement_date` int(11) NOT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`observer_id`),
  KEY `foreign key Official 2` (`village_code`),
  CONSTRAINT `foreign key Official 3` FOREIGN KEY (`village_code`) REFERENCES `election_village` (`village_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `election_allotment_details` (
  `team_id` int(11) NOT NULL AUTO_INCREMENT,
  `village_code` int(11) NOT NULL,
  `observer_id` int(11) NOT NULL,
  `official1_id` int(11) NOT NULL,
  `official2_id` int(11) NOT NULL,
  PRIMARY KEY (`team_id`),
  KEY `foreign key attotment` (`official1_id`),
  KEY `foreign key attotment 1` (`official2_id`),
  KEY `foreign key attotment 2` (`village_code`),
  KEY `foreign key attotment 3` (`observer_id`),
  CONSTRAINT `election_allotment_details_ibfk_1` FOREIGN KEY (`observer_id`) REFERENCES `election_observer` (`observer_id`),
  CONSTRAINT `election_allotment_details_ibfk_2` FOREIGN KEY (`official1_id`) REFERENCES `election_official1` (`official1_id`),
  CONSTRAINT `election_allotment_details_ibfk_3` FOREIGN KEY (`official2_id`) REFERENCES `election_official2` (`official2_id`),
  CONSTRAINT `foreign key attotment 4` FOREIGN KEY (`village_code`) REFERENCES `election_village` (`village_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
